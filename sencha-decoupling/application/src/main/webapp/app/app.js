
/**
 * @class KitchenSink.application
 * Registers the KitchenSink application and dispatches to the index controller.
 *
 *  Creates the Att.Provider instance and assigns the object to
 *  KitchenSink.provider
 *
 */
Ext.regApplication({
    name: "KitchenSink",

    launch: function() {
        Ext.dispatch({
            controller: 'index',
            action    : 'index'
        });

        this.authScope = 'SPEECH';

        //Dynamically figure out where are.
        var prefix = location.pathname.substring(0, location.pathname.lastIndexOf("/"));

        // Initialize the Provider component.
        this.provider = new Att.Provider({apiBasePath: prefix});

        this.resultOverlayTitle = new Ext.Toolbar({
            dock: 'top'
        });

        this.resultOverlay = new Ext.Panel({
              floating: true,
              modal: true,
              centered: true,
              width: Ext.is.Phone ? 320 : 500,
              height: Ext.is.Phone ? 320 : 500,
              styleHtmlContent: true,
              dockedItems: this.resultOverlayTitle,
              scroll: "both",
              cls: 'htmlcontent'
          });
    },


    /**
    * Display a formatted JSON object in a floating dialog.
    * Used by each of the API calls to display the results of the call.
    *
    * @param results {Object} JSON object to display
    * @param label {String} Title of the panel
    *
    */
    showResults: function(results, label) {
       this.resultOverlay.update( JSON.stringify(results, null, '\t'));
       this.resultOverlayTitle.setTitle(label || "Results");
      this.resultOverlay.show();

    },


    /**
    *checkLogin will call the server to see if the user has a valid auth token. If they do not
    * then it will call KitchenSink.provider.authorizeApp and redirect the user to the oauth pages
    * inside an iframe.
    *
    * @param {Function} success callback function is executed when the user can actually make api calls.
    * @param {Function} failure callback function is executed when the user does not successully logged in.
    */
    checkLogin: function(callback, failure) {


        KitchenSink.provider.isAuthorized({
            authScope : KitchenSink.authScope,
            success: function() {
                // On successful authorisation, proceed to the next page
                console.log("arguments", arguments);
                callback();
            },
            failure: function() {
                console.log("failure arguments", arguments);

                // Ask the user to login and authorize this application to process payments.
                // This will pop up an AT&T login followed by an authorisation screen.
                KitchenSink.provider.authorizeApp({
                    authScope  : KitchenSink.authScope,
                    success: function() {
                        // On successful authorisation, proceed to the next page
                        console.log("arguments", arguments);
                        callback();
                    },
                    failure: function() {
                        console.log("failure arguments", arguments);
                        failure();
                    }
                });
            }
        });
    }

});
