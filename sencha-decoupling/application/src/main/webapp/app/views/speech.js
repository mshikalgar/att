/**
 * The SpeechToText view
 * Exercises the SpeechToText API.

 This view allows the user to enter a MSISDN for a mobile device associated with their account. Only devices attached to their account can be entered. Any other number will generate an error.


 * @extends Ext.Panel
 */
KitchenSink.views.SpeechToText = Ext.extend(Ext.Panel, {

    layout: 'fit',

    /**
     * Once we send an speech message we store the ID returned by the API
     * so that we can check its delivery status.
     */
    speechToTextId: null,

     /**
       * Initializes the view placing a text input field for the MSISDN one and two and a buttons to initiate the APIs
       *
       */
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks

        this.items = [{
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'Speech To Text',
                items: {
                    xtype: 'button',
                    text: 'Back',
                    handler: function() {
                         Ext.dispatch({
                              controller: 'index',
                              action    : 'showList'
                          });
                    }
                }
            }],
            layout: 'vbox',
            fullScreen: true,
            id: 'speech-buttons',
            items: [
                { xtype: 'spacer' },
                { xtype:"form", items:[{id: 'speechOne', xtype:'textfield', label:"MSISDN"}, { xtype: 'spacer' }]},
                {
                    xtype: 'button',
                    text: 'Send Speech To Text',
                    style: 'margin-bottom: 10px;',

                    handler: self.sendSpeechToTextHandler,
                    scope: self
                },
                {
                    xtype: 'button',
                    text: 'Speech To Text Status',
                    id: 'speech-status-button',
                    style: 'margin-bottom: 10px;',

                    handler: this.speechToTextStatusHandler,
                    scope: this,
                    disabled: true
                },
                { xtype: 'spacer' }
            ]
        }];

        KitchenSink.views.SpeechToText.superclass.initComponent.apply(this, arguments);

    },



    /**
     * pulls the MSISDN from the speechOne text field and calls
     * KitchenSink.provider.sendSpeechToText
     * on a success response the results are displayed using KitchenSink.showResults
     */
    sendSpeechToTextHandler: function() {
        var self = this,
            speechOne =  Ext.getCmp("speechOne");

        // Show a 'loading' overlay mask
        self.setLoading(true);

        KitchenSink.provider.sendSpeechToText({
            address : speechOne.getValue(),
            message : 'Sencha Test Speech To Text',
            success : function(response) {
                self.setLoading(false);
                KitchenSink.showResults(response, "Speech To Text");
                self.speechToTextId = response.Id;
                Ext.getCmp('speech-status-button').enable();
            },
            failure : function(error) {
                console.log("failure", error);
                self.setLoading(false);
                Ext.Msg.alert('Error', error);
            }
        });
    },

    /**
     * After sending an SpeechToText message using sendSpeechToTextHandler a
     * speechToTextId is returned by the API. That value is stored in this.speechToTextId.
     * This method calls KitchenSink.provider.speechToTextStatus using this.speechToTextId to get delivery status of the message.
     * on a success response the results are displayed using KitchenSink.showResults
     */
    speechToTextStatusHandler: function() {
        var self = this;

        self.setLoading(true);

        KitchenSink.provider.speechToTextStatus({
            speechToTextId   : self.speechToTextId,
            success : function(response) {
                self.setLoading(false);
                KitchenSink.showResults(response, "SpeechToText Status Success");
            },
            failure : function(error) {
                self.setLoading(false);
                KitchenSink.showResults(error, "SpeechToText Status Error");
            }
        });
    }

});

Ext.reg('attSpeechToText', KitchenSink.views.SpeechToText);