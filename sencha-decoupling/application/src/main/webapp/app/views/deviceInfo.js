/**
 * The DeviceInfo view
 *
 * Exercises the Device Info API.

 This view allows the user to enter a MSISDN for a mobile device associated with their account. Only devices attached to their account can be entered. Any other number will generate an error.

 *
 * @extends Ext.Panel
 */
KitchenSink.views.DeviceInfo = Ext.extend(Ext.Panel, {

    layout: 'fit',


    /**
     * Initializes the view placing a text input field for the MSISDN and a button to initiate the API call
     *
     */
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks

        this.items = [{
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'Device Info',
                items: {
                    xtype: 'button',
                    text: 'Back',
                    handler: function() {
                        Ext.dispatch({
                             controller: 'index',
                             action    : 'showList'
                         });
                    }
                }
            }],
            layout: 'vbox',
            items: [
                { xtype: 'spacer' },
                { xtype:"form", items:[{id: 'deviceInfoInput', xtype:'textfield', label:"MSISDN:"} ]},
                {
                    xtype: 'button',
                    text: 'Device Info',

                    handler: self.deviceInfoHandler,
                    scope: self
                },
                { xtype: 'spacer' }
            ]
        }];

        KitchenSink.views.DeviceInfo.superclass.initComponent.apply(this, arguments);

    },

    /**
    * pulls the MSISDN from the deviceInfoInput text field and calls
    * KitchenSink.provider.deviceInfo
    * on a success response the results are displayed using KitchenSink.showResults
    */
    deviceInfoHandler: function() {
        var self = this,
            deviceInfoInput =  Ext.getCmp("deviceInfoInput");

        self.setLoading(true);

        KitchenSink.provider.deviceInfo({
            address : deviceInfoInput.getValue(),
            success : function(response) {
                // On successful authorisation, proceed to the next page
                self.setLoading(false);
                KitchenSink.showResults(response, "Device Info");
            },
            failure : function(error) {
                self.setLoading(false);
                Ext.Msg.alert('Error', error);
            }
        });

    }

});

Ext.reg('attDeviceInfo', KitchenSink.views.DeviceInfo);