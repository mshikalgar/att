/**
 * Exercises the Device Location API.

 This view allows the user to enter a MSISDN for a mobile device associated with their account. Only devices attached to their account can be entered. Any other number will generate an error.


 * @extends Ext.Panel
 */
KitchenSink.views.DeviceLocation = Ext.extend(Ext.Panel, {

    layout: 'fit',

    /**
     * Initializes the view placing a text input field for the MSISDN and a button to initiate the API call
     *
     */
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks

        this.items = [{
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'Device Location',
                items: {
                    xtype: 'button',
                    text: 'Back',
                    handler: function() {
                         Ext.dispatch({
                              controller: 'index',
                              action    : 'showList'
                          });
                    }
                }
            }],
            layout: 'vbox',
            items: [
                { xtype: 'spacer' },
                { xtype:"form", items:[{id: 'deviceLocationInput', xtype:'textfield', label:"MSISDN:"} ]},
                {
                    xtype: 'button',
                    text: 'Device Location',

                    handler: self.deviceLocationHandler,
                    scope: self
                },
                { xtype: 'spacer' }
            ]
        }];

        KitchenSink.views.DeviceLocation.superclass.initComponent.apply(this, arguments);

    },


    /**
    * pulls the MSISDN from the deviceLocationInput text field and calls
    * KitchenSink.provider.deviceLocation
    * on a success response the results are displayed using KitchenSink.showResults
    */
    deviceLocationHandler: function() {

        var self = this,
            deviceLocationInput =  Ext.getCmp("deviceLocationInput");

        self.setLoading(true);

        /**
         * deviceLocation also has optional options for requestedAccuracy, acceptableAccuracy and tolerance
         * Refer to the AttProvider API docs for more information
         */

        KitchenSink.provider.deviceLocation({
            address : deviceLocationInput.getValue(),
            success : function(response) {
                // On successful authorisation, proceed to the next page
                self.setLoading(false);
                KitchenSink.showResults(response, "Device Location");
            },
            failure : function(error) {
                self.setLoading(false);
                Ext.Msg.alert('Error', error);
            }
        });
    }


});

Ext.reg('attDeviceLocation', KitchenSink.views.DeviceLocation);