/**
 * The ApiList View
 *
 * List view that displays each of the APIs in a list.
 * When the user taps on an API the we show the view for that API.
 *
 *
 *
 * @extends Ext.Panel
 */
KitchenSink.views.ApiList = Ext.extend(Ext.Panel, {
    layout: 'fit',
    id: "apiList",

    // This function is run when initializing the component
    initComponent: function() {


        Ext.regModel('Section', { fields: ['section', 'page'] });

        var store = new Ext.data.JsonStore({
            model: 'Section',
            data: [
             /* { section: 'oAuth', page: 'attOAuth' },
                { section: 'Device Info', page: 'attDeviceInfo', authType: "login" },
                { section: 'Device Location', page: 'attDeviceLocation' , authType: "login" },
                { section: 'Payments', page: 'attPayments' },
                { section: 'Payment Notifications', page: 'attPaymentNotifications' }, */
                { section: 'SMS', page: 'attSMS' , authType: "app" },
                { section: 'Speech To Text', page: 'attSpeechToText' , authType: "app" } //,
              /*  { section: 'MMS', page: 'attMMS' , authType: "app" },
                { section: 'WAP', page: 'attWAP'  , authType: "app" },
                { section: 'Payment', page: 'attPayments'  , authType: "app" }*/
            ]
        });

        this.items = {
            dockedItems: [{ xtype: 'toolbar', dock: 'top', title: 'AT&T APIs' }],
            xtype: 'panel',
            layout: 'fit',
            items: {
                xtype: 'list',
                id: 'api-list',
                itemTpl: '{section}',
                store: store,
                listeners: {
                    itemtap: function(dataView, idx) {
                        console.log("one");
                        var record = dataView.getStore().getAt(idx);
                        var xtype = record.get('page');

                        if(record.get('authType') =="login"){
                            KitchenSink.checkLogin(function() {
                                console.log("API list checklogin callback.");
                                var viewport = Ext.getCmp('viewport');
                                viewport.add({xtype: xtype, id: xtype});
                                viewport.setActiveItem(xtype);
                            })

                        } else {
                            var viewport = Ext.getCmp('viewport');
                            viewport.add({xtype: xtype, id: xtype});
                            viewport.setActiveItem(xtype);
                        }

                    }
                }
            }

        };

        // This should always be called as the last item in the 'initComponent' method.
        // It ensures that superclass initComponent methods are also called
        KitchenSink.views.ApiList.superclass.initComponent.apply(this, arguments);
    }
});

Ext.reg('attApiList', KitchenSink.views.ApiList);
