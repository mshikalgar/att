/**
 * The SMS view
 * Exercises the SMS API.

 This view allows the user to enter a MSISDN for a mobile device associated with their account. Only devices attached to their account can be entered. Any other number will generate an error.


 * @extends Ext.Panel
 */
KitchenSink.views.SMS = Ext.extend(Ext.Panel, {

    layout: 'fit',

    /**
     * Once we send an sms message we store the ID returned by the API
     * so that we can check its delivery status.
     */
    smsId: null,

     /**
       * Initializes the view placing a text input field for the MSISDN one and two and a buttons to initiate the APIs
       *
       */
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks

        this.items = [{
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'SMS',
                items: {
                    xtype: 'button',
                    text: 'Back',
                    handler: function() {
                         Ext.dispatch({
                              controller: 'index',
                              action    : 'showList'
                          });
                    }
                }
            }],
            layout: 'vbox',
            fullScreen: true,
            id: 'sms-buttons',
            items: [
                { xtype: 'spacer' },
                { xtype:"form", items:[{id: 'smsOne', xtype:'textfield', label:"MSISDN One"}, {id: 'smsTwo', xtype:'textfield', label:"MSISDN Two"}]},
                {
                    xtype: 'button',
                    text: 'Send SMS',
                    style: 'margin-bottom: 10px;',

                    handler: self.sendSmsHandler,
                    scope: self
                },
                {
                    xtype: 'button',
                    text: 'Send Multi SMS',
                    style: 'margin-bottom: 10px;',

                    handler: self.sendMultiSmsHandler,
                    scope: self
                },
                {
                    xtype: 'button',
                    text: 'SMS Status',
                    id: 'sms-status-button',
                    style: 'margin-bottom: 10px;',

                    handler: this.smsStatusHandler,
                    scope: this,
                    disabled: true
                },
                {
                    xtype: 'button',
                    text: 'Receive SMS',
                    style: 'margin-bottom: 10px;',

                    handler: self.receiveSmsHandler,
                    scope: self
                },
                { xtype: 'spacer' }
            ]
        }];

        KitchenSink.views.SMS.superclass.initComponent.apply(this, arguments);

    },



    /**
     * pulls the MSISDN from the smsOne text field and calls
     * KitchenSink.provider.sendSms
     * on a success response the results are displayed using KitchenSink.showResults
     */
    sendSmsHandler: function() {
        var self = this,
            smsOne =  Ext.getCmp("smsOne");

        // Show a 'loading' overlay mask
        self.setLoading(true);

        KitchenSink.provider.sendSms({
            address : smsOne.getValue(),
            message : 'Sencha Test SMS',
            success : function(response) {
                self.setLoading(false);
                KitchenSink.showResults(response, "SMS Sent");
                self.smsId = response.Id;
                Ext.getCmp('sms-status-button').enable();
            },
            failure : function(error) {
                console.log("failure", error);
                self.setLoading(false);
                Ext.Msg.alert('Error', error);
            }
        });
    },


    /**
     * pulls the MSISDN from the smsOne and smsTwo text fields and calls
     * KitchenSink.provider.sendSms with both numbers joined with a comma.
     * on a success response the results are displayed using KitchenSink.showResults
     */
    sendMultiSmsHandler: function() {
        var self = this,
            smsOne =  Ext.getCmp("smsOne"),
            smsTwo =  Ext.getCmp("smsTwo");

        self.setLoading(true);

        KitchenSink.provider.sendSms({
            address : smsOne.getValue() + ',' + smsTwo.getValue(),
            message : 'Sencha Test Multi SMS',
            success : function(response) {
                self.setLoading(false);
                KitchenSink.showResults(response, "SMS Sent");
                self.smsId = response.Id;
                Ext.getCmp('sms-status-button').enable();
            },
            failure : function(error) {
                self.setLoading(false);
                Ext.Msg.alert('Error', error);
            }
        });
    },

    /**
     * Checks the applications shortcode 'inbox' for any messages.
     * KitchenSink.provider.receiveSms
     * on a success response the results are displayed using KitchenSink.showResults
     */
    receiveSmsHandler: function() {
        var self = this;

        self.setLoading(true);

        KitchenSink.provider.receiveSms({
            success: function(response) {
                self.setLoading(false);
                KitchenSink.showResults(response, "Receive SMS Success");
            },
            failure: function(error) {
                self.setLoading(false);
                KitchenSink.showResults(error, "Receive SMS Error");
            }
        })
    },

    /**
     * After sending an sms message using sendSmsHandler or sendMultiSmsHandler a
     * smsId is returned by the API. That value is stored in this.smsId.
     * This method calls KitchenSink.provider.smsStatus using this.smsId to get delivery status of the message.
     * on a success response the results are displayed using KitchenSink.showResults
     */
    smsStatusHandler: function() {
        var self = this;

        self.setLoading(true);

        KitchenSink.provider.smsStatus({
            smsId   : self.smsId,
            success : function(response) {
                self.setLoading(false);
                KitchenSink.showResults(response, "SMS Status Success");
            },
            failure : function(error) {
                self.setLoading(false);
                KitchenSink.showResults(error, "SMS Status Error");
            }
        });
    }

});

Ext.reg('attSMS', KitchenSink.views.SMS);