package com.sencha.att.provider;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.sencha.att.AttConstants;
import java.text.ParseException;

public class NotaryProvider {

    private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);

    private String host;
    private String clientId;
    private String clientSecret;

    public NotaryProvider(String host, String clientId, String clientSecret) {
        this.host = host;
        this.clientId = clientId;
        this.clientSecret = clientSecret;

    }

    /**
     *
     * @param toSign json object representing the payload to sign
     * @return
     */
    public TokenResponse signPaylaod(JSONObject toSign) {

        TokenResponse theReturn = null;
        String results = null;

        HttpPost post = new HttpPost(host + "/Security/Notary/Rest/1/SignedPayload?client_id=" + this.clientId + "&client_secret=" + this.clientSecret);
        results = ApiRequestManager.postJSON(toSign, post);

        if (results != null) {
            theReturn = TokenResponse.getResponse(new JSONTokener(results.toString()));
            log.info(theReturn.toString(2));
        }

        return theReturn;

    }

    /**
     * @param args
     */
    public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
        JSONTokener tk = new JSONTokener("{ \"Amount\":0.99, \"Category\":1, \"Channel\":\"MOBILE_WEB\", \"Description\":\"better than level 1\", \"MerchantTransactionId\":\"skuser2985trx20111029175423\", \"MerchantProductId\":\"level2\", \"MerchantPaymentRedirectUrl\":\"http://somewhere.com/PurchaseFulfillment\" }");
        JSONObject toSign = new JSONObject(tk);
        NotaryProvider notary = new NotaryProvider(AttConstants.HOST, AttConstants.CLIENTIDSTRING, AttConstants.CLIENTSECRETSTRING);
        TokenResponse response = notary.signPaylaod(toSign);

    }

}
