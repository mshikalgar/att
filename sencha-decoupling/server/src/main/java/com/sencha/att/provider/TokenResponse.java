package com.sencha.att.provider;

import java.text.ParseException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * An extension of JSONObject that provides methods for extracting the auth
 * token from response.
 *
 *
 */
public class TokenResponse extends JSONObject {

    public static final String TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String TOKEN_EXPIRES = "expires_in";
    public static final String REQUESTERROR1 = "RequestError";
    public static final String REQUESTERROR2 = "requestError";
    public static final String TEXT = "text";
    
    private TokenResponse() {
        super();
    }

    private TokenResponse(JSONTokener x) throws JSONException, ParseException {
        super(x);
    }


    /**
     *
     * @return true of the token is not set
     */
    public boolean hasError() {
        return optString(ServiceProviderConstants.ERROR).length() > 0;
    }

    /**
     * @return extracts the access_token from the token
     */
    public String getAccessToken() {
        return optString(TOKEN);
    }

    /**
     * @return extracts the refresh token from the token
     */
    public String getRefreshToken() {
        return optString(REFRESH_TOKEN);
    }

    /**
     * @return extracts the token expiry from the token
     */
    public String getTokenExpires() {
        return optString(TOKEN_EXPIRES);
    }

    public static TokenResponse getResponse(Exception e) {
        return getResponse(e.getMessage());
    }

    public static TokenResponse getResponse(String errorMessage) {
        TokenResponse theReturn = new TokenResponse();
        theReturn.put(ServiceProviderConstants.ERROR, errorMessage);
        return theReturn;
    }

    public static TokenResponse getResponse(JSONTokener x) {
        TokenResponse theReturn = null;
        try {
            theReturn = processError(new TokenResponse(x));
        } catch (JSONException e) {
            theReturn = getResponse(x.toString());
        } catch (Exception e) {
            theReturn = getResponse(e.getMessage());
        }
        return theReturn;
    }

    private static TokenResponse processError(TokenResponse token) {
        TokenResponse theReturn = token;
        JSONObject error = token.optJSONObject(REQUESTERROR1);
        if (error != null) {
            theReturn = new TokenResponse();
            theReturn.put(ServiceProviderConstants.APIERROR, token);
        }
        return theReturn;
    }
}
