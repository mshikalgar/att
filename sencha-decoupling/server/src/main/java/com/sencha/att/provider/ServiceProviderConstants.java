package com.sencha.att.provider;


/**
 * ServiceProviderConstants are string constants used by the classes in this package.
 * @author jason
 *
 */
public class ServiceProviderConstants {
  // Debug mode, should be set to false for productions environments
  public static final String SERVICEPROVIDERLOGGER= "ServiceProviderLogger";

  public static final String GET = "get";
  public static final String CLIENTID = "clientid";
  public static final String HOST = "host";
  public static final String CALLBACK = "callback";
  public static final String DATA = "data";
  public static final String ERROR = "error";
  public static final String RESULT = "result";
  public static final String CODE = "code";
  public static final String TOKEN = "token";
  public static final String ACCURACY = "accuracy";
  public static final String MESSAGE = "message";
  public static final String SHORTCODE = "shortcode";
  public static final String SMSID = "smsid";

  public static final String REFUNDREASON = "refundReasonText";
  public static final String REFUNDCODE = "refundReasonCode";

public static final String APIERROR = "apiError"; 
}
