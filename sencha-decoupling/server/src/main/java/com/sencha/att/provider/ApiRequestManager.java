package com.sencha.att.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * ApiRequestManager provides low level wrappers on the base HTTP methods. 
 * Setup and configuration of org.apache.http.client.HttpClient is standardized in this class.
 * 
 *
 */
public class ApiRequestManager {
	
	private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);
	
	static {init();}
	
	
	
	public static DefaultHttpClient getHTTPClient() {
		
		        try {
		         
		        	
		        	DefaultHttpClient httpclient = new DefaultHttpClient();
		        	
		        	

		   		 /**
		   		  * TODO Remove when we have a valid SSL cert.  
		   		  * Java is very particular about verifying.
		   		  * In dev we don't have a valid cert. Once in production
		   		  * this shouldn't be needed and wouldn't be a good idea to use
		   		  * Forged SSL and Man in the middle attacks are possible because
		   		  * of the next two lines:
		   		  */
		        	
		            TrustManager[] trustAllCerts = new TrustManager[]{
		    	            new X509TrustManager() {

		    	                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		    	                    return null;
		    	                }

		    	                public void checkClientTrusted(
		    	                        java.security.cert.X509Certificate[] certs, String authType) {
		    	                }

		    	                public void checkServerTrusted(
		    	                        java.security.cert.X509Certificate[] certs, String authType) {
		    	                }
		    	            }
		    	        };

		            SSLContext sc = SSLContext.getInstance("SSL");
		            sc.init(null, trustAllCerts, new java.security.SecureRandom());
		            
		            
		        	 SSLSocketFactory socketFactory = new SSLSocketFactory(sc,SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		             Scheme sch = new Scheme("https", 443, socketFactory);
		             httpclient.getConnectionManager().getSchemeRegistry().register(sch);
		             
		             BasicHttpParams params = new BasicHttpParams();
		             params.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
		             
		             
					 httpclient.setParams(params);
					 
					/*HttpHost proxy = new HttpHost("127.0.0.1", 8889);
			        httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
			        	*/
		             return httpclient;
		        } catch (Exception ex) {
		            ex.printStackTrace();
		            return null;
		        }
	
		
		
	}
	
	/**
	 * Makes an HTTP get request and returns a string of the response body.
	 * If the response returns a Location header the body is ignored and the location value
	 * is returned instead.
	 * 
	 * @param url
	 * @return
	 */
	public static String get(String url) {
		
		HttpGet get = new HttpGet(url);
		
		get.setHeader("Content-Type", "application/json");
		get.setHeader("Accept", "application/json");
		
		String results = null;
		try {
			results = execute(get);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
		
	}
	
	/** 
	 * getJson same as get() but parses the results for JSON and reutrns a JSON object
	 * @throws JSONException 
	 * 
	 */
	public static JSONObject getJson(String url) throws JSONException, ParseException {
		// TODO Auto-generated method stub
		String results = get(url);
		log.info("getJson: " + url + " results " + results);
		return new JSONObject(results);
	}
	
	/**
	 * wrapper for HTTP PUT call with no put body.
	 * @param url
	 * @return JSONObject resutls of the API call
	 * @throws JSONException
	 */
	public static JSONObject putJson(String url) throws JSONException, ParseException {
		return putJson(url, null);
	}
	
	/**
	 * wrapper for HTTP PUT where the based JSONObject is sent as the HTTP document body.
	 * @param url
	 * @param body
	 * @return
	 * @throws JSONException
	 */
	public static JSONObject putJson(String url, JSONObject body) throws JSONException, ParseException {
		String results = put(url, body);
		log.info("getJson: " + url + " results " + results);
		return new JSONObject(results);
	}
	
	public static String put(String url, JSONObject body) {
		HttpPut put = new HttpPut(url);
		
		put.setHeader("Content-Type", "application/json");
		put.setHeader("Accept", "application/json");
		StringEntity entity;
		
		String results = null;
		try {
			
			if(body != null) {
				log.info("signPaylaod " +  body.toString() );
			
				entity = new StringEntity(body.toString(),"UTF-8");

				put.setEntity(entity);
			}
			
			results = execute(put);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}
	
	/**
	 * wrapper for HTTP POST where the based JSONObject is sent as the HTTP document body.
	 * @param toPost
	 * @param post
	 * @return
	 */
	public static String postJSON(JSONObject toPost, HttpPost post) {
		
		post.addHeader("Content-Type", "application/json");
		
		StringEntity entity;
		
		String results =  null;
		try {
			

			log.info("signPaylaod " +  toPost.toString() );
			
			entity = new StringEntity(toPost.toString(),"UTF-8");

			post.setEntity(entity);
			
			results = execute(post);

			log.info("signPaylaod results" +  results );
			
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return results;
	}


	
/**
 * Takes a HttpUriRequest (post,get, put) and makes the request to the server returning the results.
 * @param request
 * @return
 * @throws IOException
 * @throws ClientProtocolException
 */
	public static String execute(HttpUriRequest request) throws IOException,
			ClientProtocolException {
		String results;
		HttpClient httpclient = getHTTPClient();
		
		
		HttpResponse response = httpclient.execute(request);
		
		Header[] headers = response.getHeaders("Location");
		
		if(headers.length > 0) {
			log.info("headers " +  headers[0]);	
			return headers[0].getValue();
		}
		
		
		HttpEntity responseEntity = response.getEntity();
		
		InputStream is = responseEntity.getContent();
		
		results = IOUtils.toString(is);
		return results;
	}
	

	 private static void disableHTTPSCertificateChecking() {
	        // Create a trust manager that does not validate certificate chains
	        TrustManager[] trustAllCerts = new TrustManager[]{
	            new X509TrustManager() {

	                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                    return null;
	                }

	                public void checkClientTrusted(
	                        java.security.cert.X509Certificate[] certs, String authType) {
	                }

	                public void checkServerTrusted(
	                        java.security.cert.X509Certificate[] certs, String authType) {
	                    System.out.println("trust running");
	                }
	            }
	        };

	        //Install the all-trusting trust manager
	        try {
	            SSLContext sc = SSLContext.getInstance("SSL");
	            sc.init(null, trustAllCerts, new java.security.SecureRandom());
	            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	        } catch (Exception e) {
	        }
	    }
	 

	private static void init() {
		

		 /**
		  * TODO Remove when we have a valid SSL cert.  
		  * Java is very particular about verifying.
		  * In dev we don't have a valid cert. Once in production
		  * this shouldn't be needed and wouldn't be a good idea to use
		  * Forged SSL and Man in the middle attacks are possible because
		  * of the next two lines:
		  */
		 disableHTTPSCertificateChecking();
		 disableHostnameVerifier();
		
	}


		private static void disableHostnameVerifier() {
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		}


		/**
		 * old style fetch content, to be removed from code as we migrate to get, post...
		 * @param url
		 * @return
		 * @throws IOException
		 * @deprecated
		 */
	public static String fetchContent(URL url) throws IOException {
		log.info("url: " + url.toString());
	      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	      conn.setDoInput(true);
	      StringBuffer response = new StringBuffer();
	      
	      log.info("conn.getResponseCode(): " +conn.getResponseCode()  +  " " + conn.getContentLength() +  " " + conn.getContentType());
	      
	      
	      if (conn.getResponseCode() < 400) {
	        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        
	        log.info("reading from buffer: ");
	        
	        String str;
	        while (null != ((str = is.readLine()))) {
	          response.append(str);
	        }
	        is.close();
	      } else {
	        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
	        

	        log.info("reading from buffer error ");
	        
	        String str;
	        while (null != ((str = is.readLine()))) {
	          response.append(str);
	        }
	        is.close();
	      }
	      log.info("response: " + response.toString());
	      
	      String responseStr = response.toString();
		return responseStr;
	}


}
