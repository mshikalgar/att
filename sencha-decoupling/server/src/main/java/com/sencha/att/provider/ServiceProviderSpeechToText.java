package com.sencha.att.provider;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.sencha.att.AttConstants;
import com.sencha.att.oauth.OAuthService;
import com.sencha.att.oauth.OAuthToken;
import com.sencha.att.rest.APIResponse;
import com.sencha.att.rest.RESTClient;
import com.sencha.att.rest.RESTException;

/**
 * Use this class to make SpeechToText API calls.
 * @author jason
 *
 */
public class ServiceProviderSpeechToText {
  private String host = "";
  private String clientId;
  private String clientSecret;
  
  private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);

  public ServiceProviderSpeechToText(String host,String clientId, String clientSecret) {
    this.host = host;
    this.clientId = clientId;
    this.clientSecret = clientSecret;

  }

  private String getHost() {
    return host;
  }


  /****
   * This method requests information about the device
   *
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param message - The message
   * @return the JSON result
   */
  /*public TokenResponse sendSpeechToText(String access_token, String tel, String message) {
    return sendSpeechToText(getHost(), access_token, tel, message);
  }*/
  
  
  protected OAuthToken getFileToken() throws RESTException {
        try {
            //final AppConfig cfg = AppConfig.getInstance();
           // final String path = "WEB-INF/token.properties";
            final String tokenFile = "token.properties";

            OAuthToken token = OAuthToken.loadToken(tokenFile);
            if (token == null || token.isAccessTokenExpired()) {
                final String clientId = AttConstants.CLIENTIDSTRING;
                final String clientSecret = AttConstants.CLIENTSECRETSTRING;
                final OAuthService service = new OAuthService(AttConstants.HOST, clientId, clientSecret);

                token = service.getToken("SPEECH");
                token.saveToken(tokenFile);
            }

            return token;
        } catch (IOException ioe) {
            throw new RESTException(ioe);
        }
    }

  /****
   * This method requests information about the device
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param message - The message
   * @return the JSON result
   */
  
  public static TokenResponse sendSpeechToText(String host, String access_token, String tel, String message) throws Exception {
	    TokenResponse theReturn = null; 
      final String endpoint = host + "/speech/v3/speechToText";
		
		System.out.println("speechContext:="+"Generic");
		//System.out.println("subContext:="+subContext);
		//System.out.println("file:="+file);
		//System.out.println("xArg:="+xArg);

      RESTClient restClient = new RESTClient(endpoint)
          .addAuthorizationHeader(access_token)
          .addHeader("Accept", "application/json")
          .addHeader("X-SpeechContext", "Generic");
      String xArg = AttConstants.XARG;
      if (xArg != null && !xArg.equals("")) {
          restClient.addHeader("X-Arg", xArg);
      }
      String subContext="Chat";
      String speechContext = "Generic";
      if (subContext != null && !subContext.equals("") && speechContext.equals("Gaming")){
			//System.out.println("************************"+file.getAbsolutePath());
          restClient.addHeader("X-SpeechSubContext",subContext);
      }
		
      File file = new File(AttConstants.AUDIO_FOLDER);
		System.out.println("Audio file:="+file);
      APIResponse apiResponse = restClient.httpPost(file);
      return parseSuccess(apiResponse.getResponseBody());
  }
  
  private static TokenResponse parseSuccess(String response)
          throws IOException, java.text.ParseException {
      String result = response;
      JSONObject object = new JSONObject(result);
      JSONObject recognition = object.getJSONObject("Recognition");
      TokenResponse sp = null;
      log.info("response: " + response.toString());
      sp = TokenResponse.getResponse(new JSONTokener(response.toString()));
      /*sp.addAttribute("ResponseID", recognition.getString("ResponseId"));
      final String jStatus = recognition.getString("Status");

      sp.addAttribute("Status", jStatus);

      if (jStatus.equals("OK")) {
          JSONArray nBest = recognition.getJSONArray("NBest");
          final String[] names = { "Hypothesis", "LanguageId", "Confidence",
                  "Grade", "ResultText", "Words", "WordScores" };
          for (int i = 0; i < nBest.length(); ++i) {
              JSONObject nBestObject = (JSONObject) nBest.get(i);
              for (final String name : names) {
                  String value = nBestObject.getString(name);
                  if (value != null) {
                      sp.addAttribute(name, value);
                  }
              }
          }
      }*/

      return sp;
  }
  
  /*public static TokenResponse sendSpeechToText(String host, String access_token, String tel, String message) {
    TokenResponse theReturn = null;
    URL url;

    try {
      url = new URL(host + "/speech/v3/speechToText");
      log.info("url: " + url.toString());
      String[] telArray = tel.split(",");

      JSONObject object = new JSONObject();

      object.put("data-binary", AttConstants.AUDIO_FOLDER);
	  //access_token = "BEARER " + access_token;
	  
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setRequestMethod("POST");
	  conn.setRequestProperty("X-SpeechContext","Generic");
      conn.setRequestProperty("Content-Type", "audio/wav");
      conn.setRequestProperty("Accept", "application/json");
	  conn.setRequestProperty("Authorization","BEARER " + access_token);
	  conn.setRequestProperty("X-Arg","test=123");
	 
	  File input = new File(AttConstants.AUDIO_FOLDER);
	  FileOutputStream f = new FileOutputStream(input);


      log.info("request: " + object.toString());
      conn.getOutputStream().write(("Content-Disposition: form-data; name=\"data-binary\";value=\""+AttConstants.AUDIO_FOLDER+"\"").getBytes());

      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {

        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));

    } catch (Exception e) {
      log.info("exception" + e.getMessage());
      e.printStackTrace();
      theReturn =  TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }*/


  /****
   * This method requests the Sms Status
   *
   * @param access_token - The Token representing the logged in user
   * @param sms_id - The Sms Id
   * @return will return a JSONObject of the device information
   */
 /* public JSONObject speechToTextStatus(String access_token, String sms_id){
    return speechToTextStatus(getHost(), access_token, sms_id);
  }
*/
  /****
   * This method requests the Sms Status
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param sms_id - The Sms Id
   * @return will return a JSONObject of the device information
   */
  /*public static TokenResponse speechToTextStatus(String host, String access_token, String sms_id){
    TokenResponse theReturn = null;


    if(AttConstants.STUBMODE) {
        String example = "{\"DeliveryInfoList\": {\"DeliveryInfo\": [{  \"Id\" : \"msg0\", \"Address\" : \"5555555555\",\"DeliveryStatus\" : \"DeliveredToTerminal\"}] ,\"ResourceURL\": \"http://api-test.san1.attcompute.com:8080/rest/sms/2/messaging/outbox/SMSc04091ed284f5b4a\" }}";
         return  TokenResponse.getResponse(new JSONTokener(example));
    }

    URL url;
    try {
      url = new URL(host + "/rest/sms/2/messaging/outbox/" + sms_id + "?access_token=" + access_token);
      log.info("url: " + url.toString());
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoInput(true);
      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {

        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));
    } catch (Exception e) {
      theReturn = TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }*/


  
}
