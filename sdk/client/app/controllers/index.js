/**
 * @class KitchenSink.controllers.index
 * Register the index controller and render the main application
 *
 * Used for global navigation.
 *
 */
Ext.regController("index", {

    index: function() {
        this.render({
            xtype: 'attKitchenSink'
        }, Ext.getBody());
    },


    showList: function() {
        console.log("KitchenSink", this,KitchenSink);
        Ext.getCmp('viewport').setActiveItem("apiList");
    },



    showLogin: function() {
      Ext.getCmp('viewport').setActiveItem(1);
    }

});
