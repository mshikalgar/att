/**
 * The WAP push view
 *
 * Exercises the WAP Push API.

 This view allows the user to enter a MSISDN for a mobile device associated with their account. Only devices attached to their account can be entered. Any other number will generate an error.

 *
 * @extends Ext.Panel
 */
KitchenSink.views.WAP = Ext.extend(Ext.Panel, {

    layout: 'fit',

     /**
       * Initializes the view placing a text input field for the MSISDN and a button to initiate the API call
       *
       */
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks

        this.items = [{
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'WAP',
                items: {
                    xtype: 'button',
                    text: 'Back',
                    handler: function() {
                        Ext.dispatch({
                             controller: 'index',
                             action    : 'showList'
                         });
                    }
                }
            }],
            layout: 'vbox',
            items: [
                { xtype: 'spacer' },
                { xtype:"form", items:[{id: 'wapOne', xtype:'textfield', label:"MSISDN One"}, {id: 'wapTwo', xtype:'textfield', label:"MSISDN Two"}]},
                {
                    xtype: 'button',
                    text: 'New Wap Push',
                    style: 'margin-bottom: 10px',

                    handler: self.newWapHandler,
                    scope: self
                },
                {
                    xtype: 'button',
                    text: 'Multi Wap Push',

                    handler: self.multiWapHandler,
                    scope: self
                },
                { xtype: 'spacer' }
            ]
        }];

        KitchenSink.views.WAP.superclass.initComponent.apply(this, arguments);

    },


    _sendWap: function(tel) {
        var self = this;
        self.setLoading(true);

        var message = '<?xml version ="1.0"?>\n<!DOCTYPE si PUBLIC "-//WAPFORUM//DTD SI 1.0//EN" "">http://www.wapforum.org/DTD/si.dtd">\n<si>\n<indication href="http://wap.uni-wise.com/hswap/zh_1/index.jsp?MF=N&Dir=23724" si-id="1">\n     CDMA Push test!!\n</indication>\n</si>';

        KitchenSink.provider.wapPush({
            address  : tel,
            message  : message,
            subject  : 'Hello From Sencha',
            priority : "High",
            success  : function(response) {
                console.log("response", response);
                self.setLoading(false);
                KitchenSink.showResults(response, "WAP Push");
            },
            failure  : function(error) {
                console.log("failure", error);
                self.setLoading(false);
                Ext.Msg.alert('Error', error);
            }
        });
    },

    /**
     * pulls the MSISDN from the wapOne text field and calls KitchenSink.provider.wapPush(...)
     * For simplicty this method has a hardcoded XML string for the send message.
     * KitchenSink.provider.wapPush
     * on a success response the results are displayed using KitchenSink.showResults
     */
    newWapHandler: function() {
        var wapOne =  Ext.getCmp("wapOne");
        this._sendWap(wapOne.getValue());
    },

    /**
     * pulls the MSISDN from the wapOne text field and calls KitchenSink.provider.wapPush(...)
     * For simplicty this method has a hardcoded XML string for the send message.
     * KitchenSink.provider.wapPush
     * on a success response the results are displayed using KitchenSink.showResults
     */
    multiWapHandler: function() {
        var wapOne =  Ext.getCmp("wapOne");
        var wapTwo =  Ext.getCmp("wapTwo");
        this._sendWap(wapOne.getValue()+ ',' + wapTwo.getValue());
    }

});

Ext.reg('attWAP', KitchenSink.views.WAP);