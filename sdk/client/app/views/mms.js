/**
 * The MMS view
 * Exercises the MMS API.

 This view allows the user to enter a MSISDN for a mobile device associated with their account. Only devices attached to their account can be entered. Any other number will generate an error.


 * @extends Ext.Panel
 */
KitchenSink.views.MMS = Ext.extend(Ext.Panel, {

    layout: 'fit',

    /**
    * Once we send an mms message we store the ID returned by the API
    * so that we can check its delivery status.
    */
    mmsId: null,


      /**
       * Initializes the view placing a text input field for the MSISDN one and two and a buttons to initiate the APIs
       *
       */
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks

        this.items = [{
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'MMS',
                items: {
                    xtype: 'button',
                    text: 'Back',
                    handler: function() {
                          Ext.dispatch({
                              controller: 'index',
                              action    : 'showList'
                          });

                    }
                }
            }],
            layout: 'vbox',
            items: [
                { xtype: 'spacer' },
                { xtype:"form", items:[{id: 'mmsOne', xtype:'textfield', label:"MSISDN One"}, {id: 'mmsTwo', xtype:'textfield', label:"MSISDN Two"}]},
                {
                    xtype: 'button',
                    text: 'Send MMS',
                    style: 'margin-bottom: 10px;',

                    handler: self.sendMmsHandler,
                    scope: self
                },
                {
                    xtype: 'button',
                    text: 'Send Multi MMS',
                    style: 'margin-bottom: 10px;',

                    handler: self.sendMultiMmsHandler,
                    scope: self
                },
                {
                    xtype: 'button',
                    text: 'MMS Status',
                    id: 'mms-status-button',

                    handler: self.mmsStatusHandler,
                    disabled: true,
                    scope: self
                },
                { xtype: 'spacer' }
            ]
        }];

        KitchenSink.views.MMS.superclass.initComponent.apply(this, arguments);

    },


    _sendMms: function(tel) {
        var self = this;
        self.setLoading(true);
        console.log("called sendMmsHandler");

        KitchenSink.provider.sendMms({
            address  : tel,
            fileId   : 'hello.jpg',
            message  : 'Hello From Sencha',
            priority :"High",
            success: function(response) {
                self.setLoading(false);
                KitchenSink.showResults(response, "MMS Sent");
                self.mmsId = response.Id;
                Ext.getCmp('mms-status-button').enable();
            },
            failure: function(error) {
                console.log("failure", error);
                self.setLoading(false);
                Ext.Msg.alert('Error', error);
            }
        });
    },

    /**
      * pulls the MSISDN from the mmsOne text field and calls
      * KitchenSink.provider.sendMMS
      *
      * NOTE because MMS is designed to send pictures and video and becuase mobile web applications
      * don't currently have access to post media files we are simply using a file stored on the
      * server file system.
      * Depending on your application needs you will need to modify the server code to allow the user
      * to choose an image or use some other logic to select an image to send.
      *
      * Please see the language specific server guide for instructions on how to customize this code.
      * on a success response the results are displayed using KitchenSink.showResults
      */
    sendMmsHandler: function() {
        var self = this,
            mmsOne =  Ext.getCmp("mmsOne");

        self.setLoading(true);
        this._sendMms(mmsOne.getValue());
    },

    /**
     * pulls the MSISDN from the mmsOne and mmsTwo text fields and calls
     * KitchenSink.provider.sendMMS  with both numbers joined with a comma.
     *  See sendMmsHandler for a special note on using MMS
     */
    sendMultiMmsHandler: function() {
        var mmsOne =  Ext.getCmp("mmsOne"),
            mmsTwo =  Ext.getCmp("mmsTwo");

        this._sendMms(mmsOne.getValue()+ ',' + mmsTwo.getValue());
    },


    /**
     * After sending an mms message using sendMmsHandler or sendMultiMmsHandler a
     * mmsId is returned by the API. That value is stored in this.mmsId.
     * This method calls KitchenSink.provider.mmsStatus using this.mmsId to get delivery status of the message.
     * on a success response the results are displayed using KitchenSink.showResults
     */
    mmsStatusHandler: function() {
        var self = this;
        self.setLoading(true);

        KitchenSink.provider.mmsStatus({
            mmsId   : self.mmsId,
            success : function(response) {
                self.setLoading(false);
                KitchenSink.showResults(response, "MMS Status Success");
            },
            failure : function(error) {
                self.setLoading(false);
                KitchenSink.showResults(response, "'MMS Status Error");
            }
        });
    }

});

Ext.reg('attMMS', KitchenSink.views.MMS);