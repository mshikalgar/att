/**
 * The OAuth view
 *
 * Executes a manual refresh of the oauth token.  This will keep the use's session alive.
 *
 * @extends Ext.Panel
 */
KitchenSink.views.OAuth = Ext.extend(Ext.Panel, {

    layout: 'fit',

    // This function is run when initializing the component
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks

        this.items = [{
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'oAuth',
                items: {
                    xtype: 'button',
                    text: 'Back',
                    handler: function() {
                         Ext.dispatch({
                              controller: 'index',
                              action    : 'showList'
                          });
                    }
                }
            }],
            layout: 'vbox',
            items: [
                { xtype: 'spacer' },
                {
                    xtype: 'button',
                    text: 'Refresh Token',

                    handler: self.refreshTokenHandler,
                    scope: self
                },
                { xtype: 'spacer' }
            ]
        }];

        KitchenSink.views.OAuth.superclass.initComponent.apply(this, arguments);

    },

    refreshTokenHandler: function() {

        var self = this;
        self.setLoading(true);

        Ext.Ajax.request({
            url: '/auth/refresh',
            method: 'POST',
            success: function(response){
                self.setLoading(false);
                Ext.Msg.alert('Refresh Token', response.responseText);
            }
        });
    }
});

Ext.reg('attOAuth', KitchenSink.views.OAuth);