/**
 * The Payments class
 * @extends Ext.Panel
 */
KitchenSink.views.Payments = Ext.extend(Ext.Panel, {

    layout: 'fit',

    // This function is run when initializing the component
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks

        this.currentTransactionId = 1;
        this.transactionAuthCode = 1;
        this.subscriptionAuthCode = 1;

        this.consumerId = "foo";

        this.items = [{
            id: 'payment-buttons',
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'Payments',
                items: {
                    xtype: 'button',
                    text: 'Back',
                    handler: function() {
                         Ext.dispatch({
                              controller: 'index',
                              action    : 'showList'
                          });
                    }
                }
            }],
            layout: 'vbox',
            defaults: {
               xtype: 'button',
               style: 'margin-bottom: 10px;'
            },
            items: [
                { xtype: 'spacer' },
                {
                    text: 'Charge User (SinglePay)',
                    handler: self.newSinglePay,
                    scope: self
                },
                {
                    text: 'Charge User (Subscription)',
                    handler: self.newSubscription,
                    scope: self
                },
                { xtype:"form", items:[{id: 'paymentAuthCode', xtype:'textfield', value: "idJ4BNj79inOmz79DzZt", label:"Auth Code:"},{id: 'paymentTxId', xtype:'textfield', value: "T1325609766337", label:"Trx ID:"} ]},
                {
                    text: 'Transaction Status',
                    handler: self.transactionStatus,
                    scope: self
                },
                {
                    text: 'Refund Transaction',
                    handler: self.refundTransaction,
                    scope: self
                },
                {
                    text: 'Subscription Details',
                    handler: self.subscriptionDetails,
                    scope: self
                },
                { xtype: 'spacer' }
            ]
        }];

        KitchenSink.views.Payments.superclass.initComponent.apply(this, arguments);

    },

    newSinglePay: function() {
        console.log("newSinglePay");
        var self = this;
        var ts = new Date().getTime();
        var charge = {
            "Amount":0.99,
            "Category":1,
            "Channel":"MOBILE_WEB",
            "Description":"D" + ts,
            "MerchantTransactionId":"T" + ts,
            "MerchantProductId":"P" + ts,
        };

        KitchenSink.provider.requestPayment({
            paymentOptions : charge,
            success : function(results) {
                console.log("success", results, results.TransactionAuthCode);
                if(results && results.TransactionAuthCode) {
                  console.log("setting this.transactionAuthCode");
                  Ext.Msg.alert('Payment Created', results.TransactionAuthCode);
                  self.transactionAuthCode = results.TransactionAuthCode;  
                  
                  Ext.getCmp("paymentAuthCode").setValue(self.transactionAuthCode);
                  
                }
            },
            failure : function() {
                console.log("fail", arguments);
                 Ext.Msg.alert('Error', "Could not complete transaction, see client and server logs.");
            }
        });

    },


    newSubscription: function() {
        console.log("newSubscription");
        var ts = new Date().getTime();
        var charge = {
            "Amount":0.99,
            "Category":1,
            "Channel":"MOBILE_WEB",
            "Description":"D" + ts,
            "MerchantTransactionId":"T" + ts,
            "MerchantProductId":"P" + ts,
            "MerchantSubscriptionIdList":"SL" + ts,
            "SubscriptionRecurringNumber":"99999",
            "SubscriptionRecurringPeriod":"MONTHLY",
            "SubscriptionRecurringPeriodAmount":"1",
            "IsPurchaseOnNoActiveSubscription":"false"
        };

        KitchenSink.provider.requestPaidSubscription({
            paymentOptions : charge,
            success : function() {
                console.log("success", arguments);
            },
            failure : function() {
                console.log("fail", arguments);
            }
        });

    },


    transactionStatus: function() {
      
        var authCode = Ext.getCmp("paymentAuthCode").getValue();
        console.log("transactionStatus", authCode);
        if(this.transactionAuthCode) {
            KitchenSink.provider.transactionStatus({
                transactionAuthCode : authCode,
                success : function(response) {
                    console.log("success", arguments);
                    KitchenSink.showResults(response, "Status");
                },
                failure : function() {
                    console.log("fail", arguments);
                }
            });
        }
    },

    refundTransaction: function() {
       var paymentTxId = Ext.getCmp("paymentTxId").getValue();
      
      
        console.log("refundTransaction");

        var details = {
            "RefundReasonCode":1,
            "RefundReasonText":"Customer was not happy"
        };

        if(this.currentTransactionId) {
            KitchenSink.provider.refundTransaction({
                transactionId : paymentTxId,
                refundOptions : details,
                success : function() {
                    console.log("success", arguments);
                },
                failure : function() {
                    console.log("fail", arguments);
                }
            });
        }
    },

    subscriptionDetails: function() {
        console.log("subscriptionDetails", this.subscriptionAuthCode);
        if(this.subscriptionAuthCode) {
            KitchenSink.provider.subscriptionDetails({
                merchantSubscriptionId : this.subscriptionAuthCode,
                consumerId : this.consumerId,
                success : function() {
                    console.log("success", arguments);
                },
                failure : function() {
                    console.log("fail", arguments);
                }
            });
        }
    }

});

Ext.reg('attPayments', KitchenSink.views.Payments);