
/**
 * @class SmsOnly.application
 * Registers the SmsOnly application and dispatches to the index controller.
 *
 *  Creates the Att.Provider instance and assigns the object to
 *  SmsOnly.provider
 *
 */
Ext.regApplication({
    name: "SmsOnly",

    launch: function() {
     

        this.provider = new Att.Provider();
        
            
        this.views.viewport = new this.views.SMS();

      
    },


});
