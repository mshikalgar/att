/**
 * The SMS view
 * Exercises the SMS API.

 This view allows the user to enter a MSISDN for a mobile device associated with their account. Only devices attached to their account can be entered. Any other number will generate an error.


 * @extends Ext.Panel
 */
SmsOnly.views.SMS = Ext.extend(Ext.Panel, {

    fullscreen: "true",

    /**
     * Once we send an sms message we store the ID returned by the API
     * so that we can check its delivery status.
     */
    smsId: null,

     /**
       * Initializes the view placing a text input field for the MSISDN one and two and a buttons to initiate the APIs
       *
       */
    initComponent: function() {

        var self = this; // We need a reference to this instance for use in callbacks
        
        
        /**
				* Declare our child UI elements
				*/
        this.items = [{
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                title: 'SMS'
            }],
            items: [
            
            /*
            * Create a form child element which contains two text fields.  for for the MSISDN and one for the message text.
            */
                { xtype:"form", 
                  items:[{id: 'smsOne', xtype:'textfield', value: "", label:"To"}, 
                          {id: 'message', xtype:'textfield', value: "Hi how are you?",  label:"Message"}]},
                          
            /*
            * Create a button which will send the sms message when pressed.
            */
                {
                    xtype: 'button',
                    text: 'Send SMS',
                    style: 'margin: 10px;',

                    /*
                    * Add a function handler so that the button will know what to do when pressed.
                    *  see sendSmsHandler() below
                    */
                    handler: self.sendSmsHandler,
                    scope: self
                },
                 /*
                  * Create a button which will check the status of the sent sms message when pressed.
                  */
                {
                    xtype: 'button',
                    text: 'SMS Status',
                    
                    /*
                    * Give this button an id so we can enable it once the message is sent.
                    */
                    id: 'sms-status-button',
                    style: 'margin: 10px;',
                    
                     /*
                      * Add a function handler so that the button will know what to do when pressed.
                      *  see smsStatusHandler() below
                      */
                    handler: this.smsStatusHandler,
                    scope: this,
                    
                    /*
                    * Set the button to disabled by default.
                    */
                    
                    disabled: true
                }            ]
        }];

        SmsOnly.views.SMS.superclass.initComponent.apply(this, arguments);

    },
    

    /**
     * pulls the MSISDN from the smsOne text field and calls
     * SmsOnly.provider.sendSms
     * on a success response the results are displayed using SmsOnly.showResults
     */
    sendSmsHandler: function() {
        var self = this,
            
            /*
            * Get references to both the MSISDN and the Message.
            */
            smsOne =  Ext.getCmp("smsOne"),
            message =  Ext.getCmp("message");
            
            
            console.log("send a sms ", smsOne.getValue(), message.getValue());
            
            
        // Show a 'loading' overlay mask
        self.setLoading(true);
       
         /*
         * call send sms with the address, message and two callback handlers.
         */
         SmsOnly.provider.sendSms({
             address : smsOne.getValue(),
             message : message.getValue(),
             success : function(response) {
                  /*
                  * When the API returns the results of the API call we will turn off the loading mask
                  * and inspect the resonse and display the result to the end user.
                  */
               
                 self.setLoading(false);
                 console.log("sms send response", response);
                 
                 /*
                 * If the response has an Id the SMS message has been sent and we can check its status.
                 * so we will store the ID in the smsId property of the view.
                 */
                 if(response && response.Id) {
                   Ext.Msg.alert('Message Sent', "Message sent to " + smsOne.getValue());
                   self.smsId = response.Id;
                   Ext.getCmp('sms-status-button').enable();
                 } else {
                   var details = "unknown";
                   
                   /**
                   * Handel errors from ATT API call. We will attempt to check the response details and 
                   * display a meaningful response to the user.
                   */
                   if(response && response.requestError && response.requestError.serviceException && response.requestError.serviceException.text) {
                     
                     details = response.requestError.serviceException.text;
                     
                     details = details.replace("%1", response.requestError.serviceException.variables);
                     
                   }
                   Ext.Msg.alert('Could not send Message', details);
                 }
                 
                
             },
             failure : function(error) {
                 console.log("failure", error);
                 self.setLoading(false);
                 Ext.Msg.alert('Error', error);
             }
         });
         
    },

    /**
     * After sending an sms message using sendSmsHandler or sendMultiSmsHandler a
     * smsId is returned by the API. That value is stored in this.smsId.
     * This method calls SmsOnly.provider.smsStatus using this.smsId to get delivery status of the message.
     * on a success response the results are displayed using SmsOnly.showResults
     */
    smsStatusHandler: function() {
        var self = this;

        self.setLoading(true);

        SmsOnly.provider.smsStatus({
                  smsId   : self.smsId,
                  success : function(response) {
                      self.setLoading(false);
                      console.log("sms Status response", response);
                      Ext.Msg.alert('Status', response.DeliveryInfoList.DeliveryInfo[0].DeliveryStatus);
                  },
                  failure : function(error) {
                      self.setLoading(false);
                      Ext.Msg.alert('Error', error);
                  }
              });
              
      
    }

});
