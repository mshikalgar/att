##
# This is an example Sinatra application demonstraiting both server and client components
# of the Sencha library for interacting with AT&T's HTML APIs
#
# Each API has a corresponding button a user can press in order to exercise that API
#
# In order to run this example code, you will need an application set up on the AT&T
# Dev Connect portal. You can sign up for an account at https://devconnect-api.att.com/
#
# Once you have logged in, set up an application and make sure all of the APIs are provisioned.
# Be sure to set your oAuth callback URL to http://127.0.0.1:4567/att/callback
#
# Update the variables below with your Application ID and Secret Key and start the server:
#
#     ruby app.rb
#

require 'rubygems'
require 'sinatra'

require File.dirname(__FILE__) + '/../lib/base'

# Store Sinatra sessions in memory rather than client side cookies
use Rack::Session::Pool

# Put the application in 'debug' mode. Set to `false` to disable debugging.
Sencha::DEBUG = :all

# define some needed constants
REDIRECT_HTML_PRE = "<!DOCTYPE html><html><head><script>window.parent.postMessage('";
REDIRECT_HTML_POST = "', '*');</script></head><body></body></html>";

SENCHA_APP_ROOT = File.dirname(__FILE__) + '/../../../client'
CONFIG_DIR = File.dirname(__FILE__) + '/../conf'
PROVIDER = "ServiceProvider"

# point the public folder to the Sencha Touch application
set :public_folder, SENCHA_APP_ROOT

# make sure Sinatra doesn't set the X-Frame-Options header
set :protection, :except => :frame_options

# get the config
$config = YAML.load_file(File.join(CONFIG_DIR, 'att-api.properties'))

# configure which port to listen to
configure do
  set :port, ARGV[0] || 4567
end

# Set up the ATT library with the Client application ID and secret. These will have been
# given to you when you registered your application on the AT&T developer site.
@@att = Sencha::ServiceProvider::Base.init(
  :provider => :att,

  :client_id => $config['apiKey'].to_s,
  :client_secret => $config['secretKey'].to_s,

  # This is the main endpoint through which all API requests are made
  :host => $config['apiHost'].to_s,

  # The address of the locally running server. This is used when a callback URL is
  # required when making a request to the AT&T APIs.
  :local_server => $config['localServer'].to_s,

  # The shortcode is the SMS number used when sending and receiving SMS and MMS messages.
  :shortcode => $config['shortCode'].to_s,

  :client_model_methods => %w(sendSms smsStatus receiveSms sendMms mmsStatus wapPush requestChargeAuth subscriptionDetails refundTransaction transactionStatus),
  :client_model_scope => $config['clientModelScope'].to_s

)

# The clientCredentialsManager needs to run in a thread so the Sinatra app can run concurrently
Thread.new do
  sleep 1 # Wait for a second to allow sinatra to start.

  while true
    puts "Starting fetch of client credentials info"
    @@att.run_get_client_model_info_in_thread
    sleep $config['clientModelRefreshSeconds']
    puts ""
  end

# exit
end



# The root URL starts off the Sencha Touch application. On the desktop, any Webkit browser
# will work, such as Google Chrome or Apple Safari. It's best to use desktop browsers
# when developing and debugging your application due to the superior developer tools such
# as the Web Inspector.
get '/' do
  File.read(File.join(SENCHA_APP_ROOT, 'index.html'))
end

# Return a json object with either 'true' or 'false' depending on whether an
# access_token has been set. This indicates whether the user is logged in.
get '/att/check' do
  content_type :json
  { :authorized => session['token'] ? true : false }.to_json
end

get '/att/payment' do
  @@att.processPaymentCallback(params)
end

# Obtain a new access token from the refresh token
post '/att/refresh' do

  response = @@att.refreshToken(session['refresh_token'])

  content_type :json

  if response.error?
    { :error => response.error }.to_json
  else

    session['token'] = response.data['access_token']
    session['refresh_token'] = response.data['refresh_token']

    response.data.to_json
  end
end




# Once the user has logged in with their credentials, they get redirected to this URL
# with a 'code' parameter. This is exchanged for an access token which can be used in any
# future calls to the AT&T APIs
get '/att/callback' do

  puts "handle callback"

  if !params[:code]
    puts 'callback : No auth code on querystring'

    content_type 'text/html'
    response = {
      :success => false,
      :msg => "No auth code"
    }

    # send back our json wrapped in html
    REDIRECT_HTML_PRE + response.to_json + REDIRECT_HTML_POST

    # don't process the rest of the codeblock
    return
  end

  puts "getToken"
  response = @@att.getToken(params[:code])

  if response.error?
    puts "callback : error in response"

    content_type 'text/html'
    response = {
      :success => false,
      :msg => "Process Callback",
      :error => response.error
    }

    # send back our json wrapped in html
    REDIRECT_HTML_PRE + response.to_json + REDIRECT_HTML_POST

  else
    puts "callback : response good"
    # Store the auth token in the session for use in future API calls
    session['token'] = response.data['access_token']
    session['refresh_token'] = response.data['refresh_token']
    puts "token = #{session[:token]}"
    puts "refresh_token = #{session[:refresh_token]}"

    content_type 'text/html'
    response = {
      :success => true,
      :msg => "Process Callback"
    }

    # send back our json wrapped in html
    REDIRECT_HTML_PRE + response.to_json + REDIRECT_HTML_POST

  end

end


# This method passes whitelisted methods through to the Provider instance
post '/att/direct_router' do
  puts "Processing /att/direct_router call"

  content_type :json
  request.body.rewind
  data = JSON.parse request.body.read
  puts "direct_router call for method #{data['method']}"

  response = {
    :type => "rpc",
    :tid => data['tid'],
    :action => data['action'],
    :method => data['method']
  }

  method_whitelist = %w(oauthUrl signPayload)

  if data['action'] == PROVIDER and method_whitelist.include?(data['method'])
    response = response.merge(@@att.send("direct_#{data['method']}", data['data']))
  elsif data['action'] == PROVIDER

    # fist, see if this method has an oauth client credential model token
    token = @@att.get_client_model_token(data['method'])

    # if not, then use the session token which should be an oauth authorize model token
    if !token
      puts "getting authorize model token from session[:token]"
      token = session[:token]
    end

    if !token
      puts "ERROR! no token found!"
      response[:error] = 'Unauthorized request'
    else
      puts "TOKEN is good, calling Provider method #{data['method']}"
      puts "token = " + token
      args = (data['data'] || []).unshift(token)
      response = response.merge(@@att.send("direct_#{data['method']}", *args))
    end
  else
    response[:error] = "Invalid direct_router action"
  end

  if response[:error]
    puts "Received ERROR response from calling Provider method #{data['method']}"
    response = response.merge({
      :type => 'exception',
      :error => response[:error]
    })
  end

  response.to_json
end
