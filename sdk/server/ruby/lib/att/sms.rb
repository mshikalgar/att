# This module contains methods pertaining to the SMS api
module Sencha
  module ServiceProvider
    module Att

      # Sends an SMS to a recipient
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} tel The MSISDN of the recipient(s). Can contain comma separated list for multiple recipients.
      #  {String} message The text of the message to send
      def sendSms(access_token, tel, message)

        url = "#{@base_url}/rest/sms/2/messaging/outbox?access_token=#{access_token}"

        # Note we pass -1 to the split method so that it will process trailing commas
        # http://gfxmonk.net/2011/09/04/ruby-s-split-function-makes-me-feel-special-in-a-bad-way.htm
        if tel =~ /,/
          address = tel.split(',',-1).collect{|t| "tel:#{t}"}
        else
          address = "tel:#{tel}"
        end

        json_post(url, {
          :Address => address,
          :Message => message
        })
      end


      # Check the status of a sent SMS
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} sms_id The unique SMS ID as retrieved from the response of the sendSms method
      def smsStatus(access_token, sms_id)
        json_get "#{@base_url}/rest/sms/2/messaging/outbox/#{URI.escape(sms_id)}?access_token=#{access_token}"
      end

      # Retrieves a list of SMSes sent to the application's short code
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      def receiveSms(access_token)
        json_get "#{@base_url}/rest/sms/2/messaging/inbox?access_token=#{access_token}&RegistrationID=#{@shortcode}"
      end

    end
  end
end
