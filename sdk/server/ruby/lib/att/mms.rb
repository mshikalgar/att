module Sencha
  module ServiceProvider
    module Att

      # Sends an MMS to a recipient
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} tel Comma separated list of MSISDN of the recipients
      #  {String} file_mime_type The MIME type of the content, eg: image/jpg
      #  {String} file_name The name of the file, eg logo.jpg
      #  {Binary} file_contents The contents of the file. Will be converted to Base64
      #  {String} priority Can be "Default", "Low", "Normal" or "High"
      #  {String} subject The subject line for the MMS
      #
      #def sendMms(access_token, tel, file_mime_type, file_name, file_contents, subject, priority)

      # temp method signature until we add support for processing file uploads
      def sendMms(access_token, tel, file_name, subject, priority)

        # hardcoded values for file_mime_type and file_contents for now
        file_mime_type = 'image/jpg'
        file_contents = File.open(File.dirname(__FILE__) + '/sencha.jpg', 'rb') { |f| f.read }

        mimeContent = MiniMime.new

        # Note we pass -1 to the split method so that it will process trailing commas
        # http://gfxmonk.net/2011/09/04/ruby-s-split-function-makes-me-feel-special-in-a-bad-way.htm
        if tel =~ /,/
          address = tel.split(',',-1).collect{|t| "tel:#{t}"}
        else
          address = "tel:#{tel}"
        end

        mimeContent.add_content(
          :type => 'application/json',
          :content => {
            :Address => address,
            :Subject => subject,
            :Priority => priority
           }.to_json
        )

        mimeContent.add_content(
          :type => file_mime_type,
          :headers => {
             'Content-Transfer-Encoding' => 'base64',
             'Content-Disposition' => 'attachment; name="' + file_name + '"'
          },
          :content_id => '<' + file_name + '>',
          :content => Base64.encode64(file_contents)
        )

        url = "#{@base_url}/rest/mms/2/messaging/outbox?access_token=#{access_token}"
        json_post_mime(url, mimeContent)
      end

      # Queries the status of a sent MMS
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} mms_id The ID of the MMS as received in the returned data when sending an MMS
      def mmsStatus(access_token, mms_id)
        json_get "#{@base_url}/rest/mms/2/messaging/outbox/#{URI.escape(mms_id)}?access_token=#{access_token}"
      end

    end
  end
end



