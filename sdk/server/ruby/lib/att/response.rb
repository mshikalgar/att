module Sencha
  module ServiceProvider
    module Att

      # This class deals with the JSON response from AT&T. It provided simple methods to
      # determine whether the response is an error and tries to extract the
      # error message if it is. If the response is not an error, it assumes it is a JSON
      # object and decodes it. Here's an example of how this class is used:
      #
      #     response = @provider.sendSms('abc123', '415-555-2425', 'Test SMS')
      #
      #     if response.error?
      #       puts "Error! " + response.error
      #     else
      #       puts "Success! " + response.data
      #     end
      #
      class Response

        # This class can is initialised with a string or exception.
        def initialize(response)

          # If the debug flag is set, output the response to the console.
          if Sencha::DEBUG == :all
            puts "DEBUG #{__FILE__} line #{__LINE__}"
            if response.respond_to?(:page)
              puts "response.page.body = "
              puts response.page.body
            elsif response.respond_to?(:body)
              puts "response.body = "
              puts response.body
            else
              puts "response.inspect = "
              puts response.inspect
            end
          end

          if response.is_a?(Exception)
            if response.respond_to?(:response_code) && response.response_code =~ /^40(1|3)$/
              # If the error has an HTTP status of 401 or 403, we have a permission error
              if response.respond_to?(:page)
                parse_response_error response.page.body
              else
                @error = "Unauthorized request"
              end
            elsif response.respond_to?(:page)
              # If the exception has a 'page' method, we actually have some response from
              # the server we can try to decode.
              parse_response_error response.page.body
            elsif response.is_a?(Timeout::Error)
              # The request timed out...
              @error = "Request timed out"
            else
              # If none of the above, pass back a 'stringified' version of the response
              @error = response.inspect
            end
          else
            # If the response isn't an exception, assume it's JSON and decode it
            @response = JSON.parse(response.body)
          end
        end

        # Used to see if the response was an error or not
        def error?
          @error ? true : false
        end

        # Returns the error message
        def error
          @error
        end

        # Returns the decoded data from the server (assuming there was no exception)
        def data
          @response
        end

        private

          # Takes a response body and tries to extract a human readable error message
          def parse_response_error(body)

            # Default the error to the entire body content
            @error = body

            begin
              # Try to parse the response as JSON
              parsed = JSON.parse(@error)
              @error = parsed
            rescue Exception => e
              # If parsing as JSON failed, try parsing as XML...
              begin
                parsed = Crack::XML.parse(@error)
                @error = parsed
              rescue Exception => e
                # If it's not JSON or XML, treat as a string.
              end
            end

            # no longer decoding the error, just sending back the entire raw json
            # TODO - need to remove the below lines once we know for sure this is what everyone wants
            return



            # If a large error JSON object is returned, attempt to strip out the 'cruft' and return a single error string
            if @error['RequestError'] && @error['RequestError']['ServiceException'] && @error['RequestError']['ServiceException']['Text']
              @error = @error['RequestError']['ServiceException']['Text']
            end

            if @error['requestError'] && @error['requestError']['serviceException'] && @error['requestError']['serviceException']['text']
              @error = @error['requestError']['serviceException']['text']
            end

            if @error['RequestError'] && @error['RequestError']['PolicyException'] && @error['RequestError']['PolicyException']['Text']
              @error = @error['RequestError']['PolicyException']['Text']
            end

            if @error['requestError'] && @error['requestError']['policyException'] && @error['requestError']['policyException']['text']
              @error = @error['requestError']['policyException']['text']
            end

          end

      end
    end
  end
end