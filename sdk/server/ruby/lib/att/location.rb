module Sencha
  module ServiceProvider
    module Att

      # Return location info for a device
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} tel MSISDN of the device to locate
      #  {Number} requested_accuracy The requested accuracy in meters
      #  {Number} acceptable_accuracy The acceptable accuracy in meters
      #  {String} tolerance The application's priority of response time versus accuracy (NoDelay, LowDelay, DelayTolerant)

      #def deviceLocation(access_token, tel, requested_accuracy = 100, acceptable_accuracy = 10000, tolerance = 'LowDelay' )
      def deviceLocation(*args)
        url = "#{@base_url}/1/devices/tel:#{URI.escape(args[1])}/location?access_token=#{args[0]}"

        # add requestedAccuracy if available
        if args.size >= 3 && args[2] != -1
          url += "&requestedAccuracy=#{args[2]}"
        end

        # add acceptableAccuracy if available
        if args.size >= 4 && args[3] != -1
          url += "&acceptableAccuracy=#{args[3]}"
        end

        # add tolerance if available
        if args.size >= 5
          url += "&tolerance=#{args[4]}"
        end

        json_get(url)

      end

    end
  end
end
