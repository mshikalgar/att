module Sencha
  module ServiceProvider
    module Att

      # Sends a WAP Push to a device
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} tel A comma separated list of MSISDNs of the recipients
      #  {String} message The message to send
      #  {String} subject The subject of the Push message
      #  {String} priority The priority of the message
      def wapPush(access_token, tel, message, subject, priority)

        mimeContent = Sencha::ServiceProvider::MiniMime.new

        # Note we pass -1 to the split method so that it will process trailing commas
        # http://gfxmonk.net/2011/09/04/ruby-s-split-function-makes-me-feel-special-in-a-bad-way.html
        if tel =~ /,/
          recipients_a = tel.split(",",-1).collect{|t| "<address>tel:#{URI.escape(t.to_s)}</address>"}
          recipients = recipients_a.join()
        else
          recipients = "<address>tel:#{URI.escape(tel.to_s)}</address>"
        end

        mimeContent.add_content(
          :type => 'text/xml',
          :content =>
            '<wapPushRequest>' + "\n" +
            '  <addresses>' + "\n" +
                 recipients + "\n" +
            '  </addresses>' + "\n" +
            '  <subject>' + URI.escape(subject) + '</subject>' + "\n" +
            '  <priority>' + URI.escape(priority) + '</priority>' + "\n" +
            '</wapPushRequest>'
        )

        mimeContent.add_content(
          :type => 'text/xml',
          :content =>
            'Content-Disposition: form-data; name="PushContent"' +  "\n" +
            'Content-Type: text/vnd.wap.si' +  "\n" +
            'Content-Length: 12' +  "\n" +
            'X-Wap-Application-Id: x-wap-application:wml.ua' +  "\n" +
            '' +  "\n" +
            message

            # The below code isn't needed now since the xml string for the message
            # needs to be built in the javascript app and sent that way and thus,
            # the message param is in xml format already
            #'<?xml version ="1.0"?>' +  "\n" +
            #'<!DOCTYPE si PUBLIC "-//WAPFORUM//DTD SI 1.0//EN" "">http://www.wapforum.org/DTD/si.dtd">' +  "\n" +
            #'<si>' +  "\n" +
            #'   <indication href="' + href + '" si-id="1">' +  "\n" +
            #'     ' + message +  "\n" +
            #'   </indication>' +  "\n" +
            #'</si>'
        )

        url = "#{@base_url}/1/messages/outbox/wapPush?access_token=#{access_token}"
        json_post_mime(url, mimeContent)

      end

    end
  end
end