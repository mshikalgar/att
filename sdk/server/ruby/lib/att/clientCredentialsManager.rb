# This module contains methods pertaining to the SMS api
module Sencha
  module ServiceProvider
    module Att

      def run_get_client_model_info_in_thread
        Thread.new do

          response = get_client_model_info

          if response.error?
            puts 'Error retrieving client model info'
          else
            # Store the token info in the session for use in future API calls
            @client_model_token = response.data['access_token']
            @client_model_expires_in = response.data['expires_in']
            @client_model_refresh_token = response.data['refresh_token']

            puts "client_model token info:"
            puts "client_model_token :         #{@client_model_token}"
            puts "client_model_expires_in :    #{@client_model_expires_in}"
            puts "client_model_refresh_token : #{@client_model_refresh_token}"

          end
        end
      end


      def get_client_model_info
        json_get "#{@base_url}/oauth/access_token?grant_type=client_credentials&client_id=#{@client_id}&client_secret=#{@client_secret}&scope=#{@client_model_scope}"
      end


      def get_client_model_token(method)

        if @client_model_methods.include?(method)
          puts "client_model_token = #{@client_model_token}"
          return @client_model_token
        else
          puts "method #{method} needs an authorization token"
          return nil
        end

      end


    end
  end
end