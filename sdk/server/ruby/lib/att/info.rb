module Sencha
  module ServiceProvider
    module Att

      # Return information on a device
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} tel MSISDN of the device to query
      def deviceInfo(access_token, tel)
        json_get "#{@base_url}/1/devices/tel:#{URI.escape(tel)}/info?access_token=#{access_token}"
      end

    end
  end
end
