module Sencha
  module ServiceProvider
    module Att

      # requestChargeAuth
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} type Payment type
      #  {Hash} payment_details A hash of payment options. Options should include:
      #    {Number} amount The payment amount
      #    {String} product_id A product identifier
      #    {String} transaction_id A unique transaction ID
      #    {String} description A description of the payment
      def requestChargeAuth(access_token, type, payment_details)
        puts 'START requestChargeAuth'
        puts access_token
        puts payment_details

        payment_details[:MerchantPaymentRedirectUrl] = "#{@local_server}/att/payment"

        signed = signPayload(payment_details)

        puts "signed.data['SignedDocument'] = "
        puts signed.data['SignedDocument']

        signed_document = signed.data['SignedDocument']
        signature = signed.data['Signature']

        puts "signed_document = "
        puts signed_document
        puts "signature = "
        puts signature

        paycall = chargeUser(type, signature, signed_document)
        return paycall

      end

      # chargeUser
      #
      def chargeUser(type, signature, signed_document)
        puts 'START chargeUser'

        if type == "SUBSCRIPTION"
          type = "Subscriptions"
        end

        if type == "SINGLEPAY"
          type = "Transactions"
        end

        url = "#{@base_url}/Commerce/Payment/Rest/2/#{type}?clientid=#{@client_id}&Signature=#{signature}&SignedPaymentDetail=#{signed_document}"
        puts "chargeUser: " + url

        response = {
            :adviceOfChargeUrl => get_header_location(url)
        }

      end


      # Sign a document
      #
      def signPayload(to_sign)
        json_post("#{@base_url}/Security/Notary/Rest/1/SignedPayload?&client_id=#{@client_id}&client_secret=#{@client_secret}", to_sign)
      end

      # Issues a refund for a transaction
      def refundTransaction(access_token, transaction_id, details)
        url = "#{@base_url}/Commerce/Payment/Rest/2/Transactions/#{transaction_id}?access_token=#{access_token}"
        puts "refundTransaction: " + url
        json_put(url, details)
      end

      # Queries the status of a transaction
      #
      # Parameters:
      #  {String} access_token The oAuth access token
      #  {String} transaction_auth_code
      def transactionStatus(access_token, transaction_auth_code)
        url = "#{@base_url}/Commerce/Payment/Rest/2/Transactions/TransactionAuthCode/#{transaction_auth_code}?access_token=#{access_token}"
        puts "transactionStatus: " + url
        json_get(url)
      end


      def subscriptionDetails(access_token, merchant_subscription_id, consumer_id)
        url = "#{@base_url}/Commerce/Payment/Rest/2/Subscriptions/#{merchant_subscription_id}/Detail/#{consumer_id}?access_token=#{access_token}"
        puts "subscriptionDetails: " + url
        json_get(url)
      end




     # Processes the callback redirect url
     # Looks for the TransactionAuthCode and if found, sends it back to the client
     # if not found, it send back the error info
     def processPaymentCallback(params)

       puts "processing payment callback"

       if !params[:TransactionAuthCode]
         puts 'callback : No TransactionAuthCode on querystring'

         content_type 'text/html'
         response = {
           :success => false,
           :error => params[:error],
           :error_reason => params[:error_reason],
           :error_description => params[:error_description]
         }

         # send back our json wrapped in html
         REDIRECT_HTML_PRE + response.to_json + REDIRECT_HTML_POST

      else

         puts "callback : response good"
         response = {
          :success => true,
          :TransactionAuthCode => params[:TransactionAuthCode]
         }

         # send back our json wrapped in html
         REDIRECT_HTML_PRE + response.to_json + REDIRECT_HTML_POST

       end

     end


      private

        def payment_options(options, subscription = false)

          %w(amount description product_id transaction_id).each do |arg|
            raise ArgumentError, "#{arg} must be set" unless options[arg.to_sym]
          end

          if subscription
            %w(recurrences recurrence_period recurrence_interval subscription_id).each do |arg|
              raise ArgumentError, "#{arg} must be set" unless options[arg.to_sym]
            end
          end

          response = {
            :amount => options[:amount],
            :category => 1,
            :channel => 'MOBILE_WEB',
            :currency => 'USD',
            :description => options[:description],
            :merchantProductID => options[:product_id],
            :merchantApplicationID => @client_id,
            :externalMerchantTransactionID => options[:transaction_id],
            :purchaseOnNoActiveSubscription => false,
            :merchantCancelRedirectUrl => "#{@local_server}/payments/cancel",
            :merchantFulfillmentRedirectUrl => "#{@local_server}/payments/deliver",
            :transactionStatusCallbackUrl => "http://astronomy.domine.co.uk:4567/payments/notify",
            :autoCommit => options[:auto_commit] || false
          }

          if subscription
            response = response.merge({
              :subscriptionRecurringNumber => options[:recurrences],
              :subscriptionRecurringPeriod => options[:recurrence_period],
              :subscriptionRecurringPeriodAmount => options[:recurrence_interval],
              :merchantSubscriptionIdList => options[:subscription_id]
            })
          end

          response

        end

    end
  end
end