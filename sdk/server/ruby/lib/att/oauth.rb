module Sencha
  module ServiceProvider
    module Att

      # Given a scope, return the corresponding AT&T oAuth URL
      #
      # Parameters:
      #  {String} scope a comma separated list of services that teh app requires access to
      def oauthUrl(scope)
        puts "*********** oauthUrl *****************"
        if scope.is_a?(Array)
          # scope will be an array when called by the direct router
          scope = scope[0]
        end
        "#{@base_url}/oauth/authorize?scope=#{scope}&client_id=#{@client_id}&redirect_uri=#{@local_server}/att/callback"
      end

      # Retrieves an access token from AT&T once the user has authorised the application and returned with an auth code
      #
      # Parameters:
      #   {String} code The code
      def getToken(code)
        json_get "#{@base_url}/oauth/access_token?grant_type=authorization_code&client_id=#{@client_id}&client_secret=#{@client_secret}&code=#{code}"
      end

      # Refreshes an access token from AT&T given a refresh token from a previous oAuth session
      #
      # Parameters:
      #   {String} refresh_token The refresh token from a previous oAuth session
      def refreshToken(refresh_token)
        json_get "#{@base_url}/oauth/access_token?grant_type=refresh_token&client_id=#{@client_id}&client_secret=#{@client_secret}&refresh_token=#{refresh_token}"
      end

    end
  end
end