require 'mechanize'
require 'logger'
require 'json'
require 'base64'

# Some of the AT&T api hosts are not signed with an authority that OpenSSL can verify so we ignore them for now
require 'openssl'
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE


require File.dirname(__FILE__) + '/clientCredentialsManager'
require File.dirname(__FILE__) + '/info'
require File.dirname(__FILE__) + '/location'
require File.dirname(__FILE__) + '/mms'
require File.dirname(__FILE__) + '/oauth'
require File.dirname(__FILE__) + '/payment'
require File.dirname(__FILE__) + '/response'
require File.dirname(__FILE__) + '/sms'
require File.dirname(__FILE__) + '/wap'

module Sencha
  module ServiceProvider
    module Att
      class Base

        # This line includes all the AT&T API methods
        include Sencha::ServiceProvider::Att

        def initialize(config)
          raise ArgumentError, "client_id must be set" unless @client_id = config[:client_id]
          raise ArgumentError, "client_secret must be set" unless @client_secret = config[:client_secret]
          raise ArgumentError, "local_server must be set" unless @local_server = config[:local_server]
          raise ArgumentError, "host must be set" unless @base_url = config[:host]
          raise ArgumentError, "shortcode must be set" unless @shortcode = config[:shortcode]
          raise ArgumentError, "client_model_scope must be set" unless @client_model_scope = config[:client_model_scope]
          raise ArgumentError, "client_model_methods must be set" unless @client_model_methods = config[:client_model_methods]


          # Mechanize is the Ruby Gem used for communicating with REST APIs. It can make REST requests
          # such as GET and POST with parameters and also handles Cookies and Redirects automatically.
          @agent = Mechanize.new

          # Give up and raise a Timeout error if the request takes longer than 20 seconds
          @agent.read_timeout = 20

          # Log requests and response headers to the console if the Debug flag is set
          if Sencha::DEBUG == :all
            @agent.log = Logger.new(STDOUT)
            puts "CALLED #{caller.join("\n")}"
            puts "\nAT&T Provider initialized.\n\n"
            puts "API endpoint:       #{@base_url}"
            puts "Client ID:          #{@client_id}"
            puts "Client Secret:      #{@client_secret}"
            puts "Local Server:       #{@local_server}"
            puts "client model scope: #{@client_model_scope}"
            puts "Shortcode:          #{@shortcode}\n\n"
          end

        end

        # Makes a GET request to the specified URL with headers set to send and receive JSON
        # returns a Response object
        def json_get(url)
          begin

            if Sencha::DEBUG == :all
              puts "GET request initiated from: "
              puts "           #{caller.select{|c| c =~ /(att|app\.rb)/}.join("\n           ")}\n\n"
            end

            Response.new @agent.get(url, [], nil, {
              "Accept" => 'application/json',
              'Content-Type' => 'application/json'
            })
          rescue Exception => e
            Response.new e
          end
        end

        #
        # returns a Location string
        def get_header_location(url)
          begin

            if Sencha::DEBUG == :all
              puts "GET request initiated from: "
              puts "           #{caller.select{|c| c =~ /(att|app\.rb)/}.join("\n           ")}\n\n"
            end

            @agent.redirect_ok = false
            result = @agent.get(url)
            result.header['location']

          rescue Exception => e
            e.inspect
          end
        end


        # Makes a POST request to the specified URL with headers set to send and receive JSON
        # returns a Response object
        def json_post(url, req)
          begin

            if Sencha::DEBUG == :all
              puts "POST request initiated from: "
              puts "           #{caller.select{|c| c =~ /(att|app\.rb)/}.join("\n           ")}\n\n"
              puts "Request Body: \n"
              puts req.to_json
            end

            Response.new @agent.post(url, req.to_json, {
              "Accept" => 'application/json',
              'Content-Type' => 'application/json'
            })
          rescue Exception => e
            Response.new e
          end
        end

        # Makes a PUT request to the specified URL with headers set to send and receive JSON
        # returns a Response object
        def json_put(url, req)
          begin

            if Sencha::DEBUG == :all
              puts "PUT request initiated from: "
              puts "           #{caller.select{|c| c =~ /(att|app\.rb)/}.join("\n           ")}\n\n"
              if req
                puts "Request Body: \n"
                puts req.to_json
              end
            end

            Response.new @agent.put(url, req, {
              "Accept" => 'application/json',
              'Content-Type' => 'application/json'
            })
          rescue Exception => e
            Response.new e
          end
        end

        # Special version of the json_post method in which the content is MIME encoded
        def json_post_mime(url, mimeContent)
          begin

            if Sencha::DEBUG == :all
              puts "POST request initiated from: "
              puts "           #{caller.select{|c| c =~ /(att|app\.rb)/}.join("\n           ")}\n\n"
              puts "Request Body: \n"
              puts mimeContent.content
            end

            Response.new @agent.post(url, mimeContent.content, {
              'Accept' => 'application/json',
              'Content-Type' => mimeContent.header
            })
          rescue Exception => e
            Response.new e
          end
        end


        def get_access_token(method)
          puts '*** in get_access_token ***'
          puts method
          puts session[:client_model_token]
          session[:client_model_token]
        end

        # If a method starts with `direct_` then assume it exists and wrap the result for
        # use with an Ext.Direct style response.
        def method_missing(m, *args, &block)

          if m.to_s =~ /^direct_(.*)$/
            response = self.send($1.to_sym, *args, &block)

            if response.is_a?(Sencha::ServiceProvider::Att::Response)
              if response.error?
                { :error => response.error }
              else
                { :result => response.data }
              end
            else
              { :result => response }
            end
          else
            { :error => "No such method" }
          end

        end

        # For use in testing. Provides direct access to the Mechanize instance
        def agent
          @agent
        end

      end
    end
  end
end
