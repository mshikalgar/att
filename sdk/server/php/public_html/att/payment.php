<?php

require_once("../config.php");

  $authCode = trim($_GET['TransactionAuthCode']);
	$response = array();
  if ($authCode) {
    $response["success"] = true;
		$response["TransactionAuthCode"] = $authCode;
		

  } else {
   	$error = $_GET["error"];
		$error_reason = $_GET["error_reason"];
		$error_description = $_GET["error_description"];
		
		$response["success"] = false;
		$response["error"] = $error;
		$response["error_reason"] = $error_reason;
		$response["error_description"] = $error_description;
		

  } 

	$response_json = json_encode($response);
	echo  REDIRECT_HTML_PRE . $response_json . REDIRECT_HTML_POST;

?>
