<?php

require_once("../config.php");

if ($_GET['code']) {
  # Once the user has logged in with their credentials, they get redirected to this URL
  # with a 'code' parameter. This is exchanged for an access token which can be used in any
  # future calls to the AT&T APIs
  $code = trim($_GET['code']);

  if (!$code) {
    # if there is no code then redirect to an HTML template located in ./views/error.php, passing local variables which can be accessed from inside the template
    #header("Location: /callback.html?error='No auth code'");

		echo  REDIRECT_HTML_PRE . '{"success": false,"msg": "No auth code "}'. REDIRECT_HTML_POST;

  } else {
    $response = $provider->getToken($code);

    if ($response->isError()) {
      #header("Location: /callback.html?error='" . $response->error() . "'");
			echo  REDIRECT_HTML_PRE . '{"success": false,"msg": "Could not fetch auth token"}'. REDIRECT_HTML_POST;

    } else {
      # Store the auth token in the session for use in future API calls
      $_SESSION['token'] = $response->data()->access_token;
      $_SESSION['refresh_token'] = $response->data()->refresh_token;

      # Render the `callback` template in ./views/callback.erb
      # This template will attempt to run the `successCallback` function
      # in the parent iframe.
      #header("Location: /callback.html");
			
				echo  REDIRECT_HTML_PRE . '{"success": true,"msg": "Process Callback"}' . REDIRECT_HTML_POST;
    }
  }
}
?>
