<?php
require_once("../config.php");
if ($_SERVER['REQUEST_METHOD'] === "POST") {

  # define our PROVIDER constant
  define("PROVIDER", "ServiceProvider");

  # get raw post data
  $data = json_decode(trim(file_get_contents("php://input")));


  $response = (object) array(
    "type" => "rpc",
    "tid" => $data->tid,
    "action" => $data->action,
    "method" => $data->method
  );

  $method_whitelist = array("oauthUrl");
  $requiring_access_whitelist = array("deviceInfo", "deviceLocation", "sendSms", "smsStatus", "receiveSms", "mmsStatus", "wapPush", "sendMms", "requestChargeAuth", "subscriptionDetails", "refundTransaction", "transactionStatus");
  $client_credentials = array("sendSms", "smsStatus", "receiveSms", "mmsStatus", "wapPush", "sendMms",  "requestChargeAuth", "subscriptionDetails", "refundTransaction", "transactionStatus");

  # This passes whitelisted methods through to the Provider instance
  if ($data->action === PROVIDER && in_array($data->method, $method_whitelist)) {
    $response = (object) array_merge((array) $response, (array) $provider->{"direct_" . $data->method}($data->data[0]));

  } elseif ($data->action === PROVIDER && in_array($data->method, $requiring_access_whitelist)) {

        $token = $_SESSION['token'];
#echo "check";

        if(in_array($data->method, $client_credentials)) {
        	#echo "using client credentials";
            if($_SESSION['client_token']) {
                $token =  $_SESSION['client_token'];
                #echo "session has client credentials " . $token;
            } else {
                $token = $provider->getClientCredentials("")->data()->access_token;
                #echo "fetched token " . $token;
                $_SESSION['client_token'] = $token;
            }

        }


    if (!$token) {
      $response->error = "Unauthorized request";

    } else {
      if (!$data->data) { //some methods like receiveSms have a null value for $data->data;
        $data->data = array();
      }

      # always push the token to the front of the data array
      array_unshift($data->data, $token);

      # the router makes dynamic function calls with a variable number of arguments
      $response = (object) array_merge((array) $response, (array) call_user_func_array(array($provider, "direct_" . $data->method), $data->data));
    }

  } else {
    $response->error = "Unrecognized method";
  }

  if (isset($response->error)) {
    $response = (object) array_merge((array) $response, array("type" => "exception", "error" => $response->error));
  }

  echo json_encode($response);
}
?>
