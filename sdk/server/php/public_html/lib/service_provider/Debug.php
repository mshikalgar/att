<?php

class Debug {
  private static $fp;

  public static function init($needsPointer = 0) {
    self::$fp = fopen(DEBUG_LOGGER, "a");

    # return the file pointer if needed, eg for a cURL request
    if ($needsPointer) {
      return self::$fp;
    }
  }

  public static function write($output) {
    fwrite(self::$fp, $output);
  }

  public static function dumpBacktrace($nl = "\n") {
    $str = $nl . "*******************************************";
    $str .= $nl . "Debug backtrace begin" . $nl;
    foreach (debug_backtrace() as $key => $value) {
      # place any functions to ignore in the array below
      if (!in_array($value['function'], array("dumpBacktrace", "__call", "call_user_func_array"))) {
        # skip if it's a direct_ function, eg direct_deviceInfo, etc
        if (!stristr($value['function'], "direct_")) {
          $str .= $nl . "function: {$value['function']}; file: {$value['file']}; line: {$value['line']}";
        }
      }
    }
    $str .= $nl . $nl . "Debug backtrace end";
    $str .= $nl . "*******************************************" . $nl;
    self::write($str);
  }

  public static function end() {
    fclose(self::$fp);
  }
}
?>
