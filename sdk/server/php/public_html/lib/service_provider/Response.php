<?php
# This class deals with the JSON response from AT&T. It provided simple methods to
# determine whether the response is an error and tries to extract the
# error message if it is. If the response is not an error, it assumes it is a JSON
# object and decodes it. Here's an example of how this class is used:
#
#     $response = $provider.sendSms('abc123', '415-555-2425', 'Test SMS')
#
#     if ($response.isError()) {
#       return "Error! " + $response.error()
#     } else {
#       return "Success! " + response.data()
#     }
#
class Response {
  private $response;
  private $headers;
  private $error = false;

  public function __construct($response) {
    if ($response instanceof Exception) {
      # if it's an exception object then grab the error message
      $this->error = $response->getMessage();

    } else {
      if (in_array($response['headers']['http_code'], array(401, 403))) {
        # check if it's an unauthrized request
        $this->error = $response['headers']['http_code'] . ": Unauthorized request";

      } else {
        # else parse the response, it could be either JSON or XML
        $this->response = $this->parse_response($response['body']);
      }
      # store the headers (curl info)
      $this->headers = $response['headers'];
    }
  }

  # Used to see if the response was an error or not
  public function isError() {
    return $this->error ? true : false;
  }

  # Returns the error message
  public function error() {
    return  $this->error;
  }

  # Returns the decoded data from the server (assuming there was no exception)
  public function data() {
    return $this->response;
  }

  # Returns the headers from the server (assuming there was no exception)
  public function headers() {
    return $this->headers;
  }

  private function parse_response($parsed) {
    $orig = $parsed;

    # Try to parse the response as JSON
    $parsed = json_decode($parsed);

    if (is_null($parsed)) {
      # If parsing as JSON failed, try parsing as XML...
      $parsed = simplexml_load_string("<xml>$orig</xml>");

    } elseif (is_null($parsed)) {
      # If it's not JSON or XML, treat as a string.
      return $orig;
    }


    return $parsed;
  }
}
?>
