<?php

abstract class Base {
  private function makeRequest($type, $url, $contentType, $postfields = null) {
    try {
      $curl = curl_init($url);
      $options = array(
        CURLOPT_HTTPHEADER => array("Content-Type: $contentType", "Accept: application/json"),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false
      );

      curl_setopt_array($curl, $options);

      if ($type === "POST") {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
      }

      # if debugging, set the options before executing the request
      if (DEBUG) {
        $fp = Debug::init(1);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_STDERR, $fp);
      }

      $curl_response = curl_exec($curl);
      $curl_info = curl_getinfo($curl);


      # if debugging, capture the response body after the request has been sent but before the curl instance is closed
      if (DEBUG) {
        if ($type === "POST") {
          DEBUG::write("\n-------------------post data----------------------\n\n$postfields\n");
        }

        DEBUG::write("\n-------------------response body----------------------\n\n$curl_response\n");
        DEBUG::dumpBacktrace();
        DEBUG::end();
      }

      curl_close($curl);

      return new Response(array(
        "body" => $curl_response,
        "headers" => $curl_info
      ));

    } catch (Exception $e) {
      return new Response($e);
    }
  }



	private function makeRequestIncludeHeaders($type, $url, $contentType, $postfields = null) {
    try {
      $curl = curl_init($url);
      $options = array(
        CURLOPT_HTTPHEADER => array("Content-Type: $contentType", "Accept: application/json"),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_MAXREDIRS => 0,
				CURLOPT_HEADER => true
      );

      curl_setopt_array($curl, $options);

      if ($type === "POST") {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
      }

      # if debugging, set the options before executing the request
      if (DEBUG) {
        $fp = Debug::init(1);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_STDERR, $fp);
      }

      $curl_response = curl_exec($curl);
      $curl_info = curl_getinfo($curl);



		 $pattern = '#HTTP/\d\.\d.*?$.*?\r\n\r\n#ims';

		 # Extract headers from response
		
     preg_match_all($pattern, $curl_response, $matches);
     $headers_string = array_pop($matches[0]);
     $headersRaw = explode("\r\n", str_replace("\r\n\r\n", '', $headers_string));

$headers = array();

		 # Convert headers into an associative array
     foreach ($headersRaw as $header) {
         preg_match('#(.*?)\:\s(.*)#', $header, $matches);
         $headers[$matches[1]] = $matches[2];
     }

	


return $headers;

    } catch (Exception $e) {
      return new Response($e);
    }
  }

  public function base64Encode($img_file) {
    $img_binary = fread(fopen($img_file, "r"), filesize($img_file));
    return base64_encode($img_binary);
  }

  public function json_get($url) {
    return $this->makeRequest("GET", $url, "application/json");
  }

  public function json_getHeaders($url) {
    return $this->makeRequestIncludeHeaders("GET", $url, "application/json");
  }


  public function json_post($url, $data) {
    return $this->makeRequest("POST", $url, "application/json", json_encode($data));
  }

  protected function json_post_mime($url, $mimeContent) {
    return $this->makeRequest("POST", $url, $mimeContent->header(), $mimeContent->content());
  }
}
?>
