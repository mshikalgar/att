<?php

    function exception_handler($exception) {
     echo "Fatal error: " , $exception->getMessage(), "\n";
    }
    set_exception_handler("exception_handler");

    # the base class for the AT&T provider
    class Sencha_ServiceProvider_Base_Att extends Base {

        private $client_id = "";
        private $client_secret = "";
        private $local_server = "";
        private $base_url = "";
        private $shortcode = "";
				private $clientModelScope = "";


        public function __construct($config) {

            if (!$config['apiKey']) throw new Exception("apiKey must be set");
            if (!$config['secretKey']) throw new Exception("secretKey must be set");
            if (!$config['localServer']) throw new Exception("localServer must be set");
            if (!$config['apiHost']) throw new Exception("apiHost must be set");
            if (!$config['shortCode']) throw new Exception("shortcode must be set");

            $this->client_id = $config['apiKey'];
            $this->client_secret = $config['secretKey'];
            $this->local_server = $config['localServer'];
            $this->base_url = $config['apiHost'];
            $this->shortcode = $config['shortCode'];

						$this->clientModelScope = $config['clientModelScope'];

            if (DEBUG) {
                DEBUG::init();
                DEBUG::write("\nAT&T Provider initialized.\n");
                DEBUG::dumpBacktrace();
                DEBUG::write("API endpoint: $this->base_url\nClient ID: $this->client_id\nClient Secret: $this->client_secret\nLocal Server: $this->local_server\nShortcode: $this->shortcode\n\n");
                DEBUG::end();
            }
        }


        # Given a scope, return the corresponding AT&T oAuth URL
        #
        # Parameters:
        #  {String} scope a comma separated list of services that teh app requires access to
        public function oauthUrl($scope) {
            # Generate the oauth redirect URL depending on the scope requested by the client.
            # The scope is specified as a parameter in the GET request and passed to the provider
            # library to obtain the appropriate OAuth URL
            if (is_array($scope)) {
                # $scope will be an array when called by the direct router
                $scope = $scope[0];
            }
            return "$this->base_url/oauth/authorize?scope=$scope&client_id={$this->client_id}&redirect_uri={$this->local_server}/att/callback";
        }


        # Retrieves and access token from AT&T once the user has authorised the application and returned with an auth code
        #
        # Parameters:
        #   {String} code The code
        public function getToken($code) {
            # Retrieves and access token from AT&T once the user has authorised the application and returned with an auth code
            return $this->json_get("$this->base_url/oauth/access_token?grant_type=authorization_code&client_id={$this->client_id}&client_secret={$this->client_secret}&code=$code");
        }


        # Refreshes an access token from AT&T given a refresh token from a previous oAuth session
        #
        # Parameters:
        #   {String} refresh_token The refresh token from a previous oAuth session
        public function refreshToken($refresh_token) {
            # Refreshes an access token from AT&T given a refresh token from a previous oAuth session
            return $this->json_get("$this->base_url/oauth/access_token?grant_type=refresh_token&client_id={$this->client_id}&client_secret={$this->client_secret}&refresh_token=$refresh_token");
        }


        # Return information on a device
        #
        # Parameters:
        #  {Hash} data A hash of Device Info options. Options should include:
        #    {String} token The oAuth access token
        #    {String} tel MSISDN of the device to query
        public function deviceInfo($data) {
            return $this->json_get("$this->base_url/1/devices/tel:$data[1]/info?access_token=$data[0]");
        }


        public function getClientCredentials($scope) {
						$url = "$this->base_url/oauth/access_token?grant_type=client_credentials&client_id={$this->client_id}&client_secret={$this->client_secret}&scope={$this->clientModelScope}";
            return $this->json_get($url);
        }


        # Return location info for a device
        #
        # Parameters:
        #  {Hash} data A hash of Device Info options. Options should include:
        #    {String} token The oAuth access token
        #    {String} tel MSISDN of the device to locate
        #    {Number} accuracy The accuracy
        public function deviceLocation($data) {

            $url = "$this->base_url/1/devices/tel:$data[1]/location?access_token=$data[0]";

            if (intval($data[2]) > 0) {
                $url = $url . "&requestedAccuracy=$data[2]";
            }

            if (intval($data[3]) > 0) {
                $url = $url . "&acceptableAccuracy=$data[3]";
            }

            if (strlen($data[4]) > 0) {
                $url = $url . "&tolerance=$data[4]";
            }

            return $this->json_get($url);
        }


        # Sends an SMS to a recipient
        #
        # Parameters:
        #  {Hash} data A hash of SMS options. Options should include:
        #    {String} token The oAuth access token
        #    {String} tel The MSISDN of the recipient(s). Can contain comma separated list for multiple recipients.
        #    {String} message The text of the message to send
        public function sendSms($data) {
            $address = $data[1];
            if (strstr($address, ",")) {
                # if it's csv, split and iterate over each value prepending each value with "tel:"
                $address = split(",", $address);
                foreach ($address as $key => $value) {
                    $address[$key] = "tel:$value";
                }
            } else {
                $address = "tel:$address";
            }

            return $this->json_post("$this->base_url/rest/sms/2/messaging/outbox?access_token=$data[0]", array("Address" => $address, "Message" => $data[2]));
        }


        # Check the status of a sent SMS
        #
        # Parameters:
        #  {Hash} data A hash of SMS options. Options should include:
        #    {String} token The oAuth access token
        #    {String} tel The unique SMS ID as retrieved from the response of the sendSms method
        public function smsStatus($data) {
            return $this->json_get("$this->base_url/rest/sms/2/messaging/outbox/$data[1]?access_token=$data[0]");
        }


        # Retrieves a list of SMSes sent to the application's short code
        #
        # Parameters:
        #  {Hash} data A hash of SMS options. Options should include:
        #    {String} token The oAuth access token
        public function receiveSms($data) {
            return $this->json_get("$this->base_url/rest/sms/2/messaging/inbox?access_token=$data[0]&RegistrationID=$this->shortcode&from=json");
        }



        # Retrieves a list of SMSes sent to the application's short code
        #
        # Parameters:
        # {Hash} data A hash of SMS options. Options should include:
        #    {String} token The oAuth access token
        public function requestChargeAuth($data) {

            $type = $data[1];
            $paymentDetails = $data[2];

            if($type == "SUBSCRIPTION") {
                $type = "Subscriptions";
            }

            if($type == "SINGLEPAY") {
                $type = "Transactions";
            }


            $paymentDetails->MerchantPaymentRedirectUrl = "{$this->local_server}/att/payment";


            $signed = $this->notary($paymentDetails);

            #var_dump($signed);

            $doc = $signed->data()->SignedDocument;

            $sig = $signed->data()->Signature;

            #echo"sig $sig **";

            $url = "$this->base_url/Commerce/Payment/Rest/2/$type?clientid={$this->client_id}&Signature=$sig&SignedPaymentDetail=$doc";

            $results = $this->json_getHeaders($url);

            $temp = array();

            $temp["adviceOfChargeUrl"] = $results["Location"];

            return $temp;
        }


        # Notary
        #  Sign a document
        public function notary($toSign) {
            return $this->json_post("$this->base_url/Security/Notary/Rest/1/SignedPayload?&client_id={$this->client_id}&client_secret={$this->client_secret}", $toSign);
        }



        # Queries the status of a transaction
        #
        public function transactionStatus($data) {
						$url = "$this->base_url/Commerce/Payment/Rest/2/Transactions/TransactionAuthCode/$data[1]?access_token=$data[0]";
	          return $this->json_get($url);
        }



        public function sendMms($data) {
            #, $file_mime_type, $file_name, $encoded_file, $priority, $subject
            #echo "Tel $tel image $image";

            $encoded_file = $this->base64Encode("./sencha.jpg");

            #echo "encoded_file" . $encoded_file;

            $response = $this->_sendMms($data[0], $data[1] , "image/jpeg", "sencha.jpg", $encoded_file, $data[3], $data[4]);

            return $response;

            //echo json_encode($response->data());

        }



        # Sends an MMS to a recipient
        #
        # Parameters:
        #  {String} token The oAuth access token
        #  {String} tel Comma separated list of MSISDN of the recipients
        #  {String} file_mime_type The MIME type of the content, eg: image/jpg
        #  {String} file_name The name of the file, eg logo.jpg
        #  {Binary} endcoded_file The contents of the file, converted to Base64
        #  {String} priority Can be "Default", "Low", "Normal" or "High"
        #  {String} subject The subject line for the MMS
        public function _sendMms($token, $tel, $file_mime_type, $file_name, $encoded_file,  $subject, $priority) {
            if (strstr($tel, ",")) {
                # if it's csv, split and iterate over each value prepending each value with "tel:"
                $tel = split(",", $tel);
                foreach ($tel as $key => $value) {
                    $tel[$key] = "\"tel:$value\"";
                }
                # json-encoded array
                $tel = "[" . join(",", $tel) . "]";
            } else {
                $tel = "\"tel:$tel\"";
            }

            #echo "$token, $tel, $file_mime_type, $file_name, $encoded_file, $priority, $subject";

            $mimeContent = new MiniMime();
            $mimeContent->add_content(array(
                "type" => "application/json",
                "content" => "{ 'Address' : $tel, 'Subject' : '$subject', 'Priority': '$priority' }"
            ));

            #var_dump($mimeContent);

            $mimeContent->add_content(array(
                "type" => $file_mime_type,
                "headers" => array(
                    "Content-Transfer-Encoding" => "base64",
                    "Content-Disposition" => "attachment; name=$file_name"
                ),
                "content_id" => "<$file_name>",
                "content" => $encoded_file
            ));

            #    var_dump($mimeContent);
            #    echo "$this->base_url/rest/mms/2/messaging/outbox?access_token=$token";
            return $this->json_post_mime("$this->base_url/rest/mms/2/messaging/outbox?access_token=$token", $mimeContent);
        }


        # Queries the status of a sent MMS
        #
        # Parameters:
        #  {Hash} data A hash of SMS options. Options should include:
        #    {String} token The oAuth access token
        #    {String} mms_id The ID of the MMS as received in the returned data when sending an MMS
        public function mmsStatus($data) {
            return $this->json_get("$this->base_url/rest/mms/2/messaging/outbox/$data[1]?access_token=$data[0]");
        }


        public function wapPush($data) {

            $response = $this->_wapPush($data[0], $data[1], $data[2], $data[3], $data[4]);
            return $response;

        }


        # Sends a WAP Push to a device
        #
        # Parameters:
        #  {String} token The oAuth access token
        #  {String} tel A comma separated list of MSISDNs of the recipients
        #  {String} message The message to send
        #  {String} subject The subject of the Push message
        #  {String} priority The priority of the message
        public function _wapPush($token, $tel, $message, $subject, $priority) {
            $mimeContent = new MiniMime();

            $tel = split(",", $tel);
            $tel = join("</address><address>tel:", $tel);

            $mimeContent->add_content(array(
                "type" => "text/xml",
                "content" =>
                    "<wapPushRequest>\n" .
                    "  <addresses>\n" .
                    "     <address>tel:$tel</address>\n" .
                    "  </addresses>\n" .
                    "  <subject>$subject</subject>\n" .
                    "  <priority>$priority</priority>\n" .
                    "</wapPushRequest>"
            ));

            $mimeContent->add_content(array(
                "type" => "text/xml",
                "content" =>
                    "Content-Disposition: form-data; name=\"PushContent\"\n" .
                    "Content-Type: text/vnd.wap.si\n" .
                    "Content-Length: 12\n" .
                    "X-Wap-Application-Id: x-wap-application:wml.ua\n" .
                    "\n" .
                    $message
            ));

            return $this->json_post_mime("$this->base_url/1/messages/outbox/wapPush?access_token=$token", $mimeContent);
        }

        public function __call($name, $args) {
            preg_match("/^direct_(.*)$/", $name, $matches);
            if ($method = $matches[1]) {
                # $args is an array of the passed params (this is important to know since we're calling methods using call_user_func_array())
                # also, return value must be cast to an object as it will then be merged into another object
                $response = $this->$method($args);
                if ($response instanceof Response) {
                    if ($response->isError()) {
                        return (object) array("error" => $response->error());
                    } else {
                        return (object) array("result" => $response->data());
                    }
                } else {
                    # for APIs like oauthUrl that only return a string and don't wrap a cURL response in a Response object, $response will be a string
                    return (object) array("result" => $response);
                }
            } else {
                return (object) array("error" => "No such method");
            }
        }
    }
?>
