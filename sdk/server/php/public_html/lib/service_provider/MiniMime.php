<?php
# This class produces Mime content strings for multipart data
class MiniMime {

  private $split = "";
  private $contents = array();

  public function __construct() {
    $this->split = "----=_Part_0_" . rand() . "." . ((string) time());
  }

  public function add_content($configs) {
    $result = "Content-Type: {$configs['type']}";

    # Set the Content-ID header
    if (count($this->contents) == 0 && !$configs['content_id']) {
      $result .= "\nContent-ID: <part0@sencha.com>";
      $result .= "\nContent-Disposition: form-data; name=\"root-fields\"";
    } else {
      $content_id = $configs['content_id'];
      if (!$content_id) {
        $content_id = "<part" . (count($this->contents) + 1) . "@sencha.com>";
      }
      $result .= "\nContent-ID: $content_id";
    }

    if ($configs['headers']) {
      foreach ($configs['headers'] as $key => $value) {
        $result .= "\n$key: $value";
      }
    }

    $result .= "\n\n{$configs['content']}";
    if (substr($configs['content'], -1) != "\n") {
      $result .= "\n";
    }
    array_push($this->contents, $result);
  }

  public function header() {
    return 'multipart/form-data; type="application/json"; start="<part0@sencha.com>"; boundary="' . $this->split . '"';
  }

  public function content() {
    return "--$this->split\n" . implode("--$this->split\n", $this->contents) . "\n--$this->split--\n";
  }
}
?>
