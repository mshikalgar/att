package com.sencha.att.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONTokener;

import com.sencha.att.AttConstants;


/**
 * ClientCredentialsManager provides a valid auth token for use on API services that
 * allow ClientCredentials auth tokens.  At present time those apis include SMS, MMS and WapPush
 * 
 * Once created ClientCredentialsManager will fetch an auth token using the supplied credentials and scope.
 * On a timer it will fetch a new auth_token before the token expires so that the server will always have a valid token
 * 
 * 
 *
 */
public class ClientCredentialsManager {

	private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);


	private String host;
	private String apiKey;
	private String scope;
	private String apiSecret;
	private Timer timer;

	private String currentAuthToken; 

	/**
	 * Create a ClientCredentialsManager for a application.  Depending on your application you may want more than
	 * one of these if you have more than on application registered with ATT. 
	 * 
	 * ClientCredentialsManager can be configured to fetch new tokens automatically on a timer set by refreshSeconds.
	 * If you set timedFetch to true then it will fetch a new token every refreshSeconds.
	 * 
	 * @param host   where to make the call
	 * @param apiKey  your app key
	 * @param apiSecret your app secret
	 * @param scope  scopes to authorize for
	 * @param refreshSeconds  how often to fetch a new key
	 * @param timedFetch  true the app calls the api every refreshSeconds, false means fetchToken() must be called manually. 
	 */
	public ClientCredentialsManager(String host, String apiKey, String apiSecret, String scope, int refreshSeconds, boolean timedFetch) {
		this.host = host;
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
		this.scope = scope;

		if(AttConstants.STUBMODE) {
			log.warning("In STUBMODE ClientCredentialsManager is diabled");
			return;
		}
		
		log.info("ClientCredentialsManager started will fetch " + this.scope + " every " + refreshSeconds + " seconds.");
		
		if(timedFetch) {
			this.timer = new  Timer();
			this.timer.schedule(new FetchToken(), 0, refreshSeconds * 1000);
		}
	}

	/**
	 * fetchToken will attempt to get an authToken from ATT. It will block execution until the api request completes.   
	 * 
	 * @return
	 * @throws Exception
	 */
	public String fetchToken() throws Exception {
		TokenResponse response = null;
		URL url;

		url = new URL(host + "/oauth/access_token?grant_type=client_credentials&client_id=" + apiKey + "&client_secret=" + apiSecret + "&scope="
				+ scope);
		String responseStr = ApiRequestManager.fetchContent(url);

		response = TokenResponse.getResponse(new JSONTokener(responseStr));

		log.info("theReturn: " + response.toString(2));
		
		System.out.println(response.toString(2));
		String access_token = response.getAccessToken();

		if(access_token != null  && access_token.length() > 0) {
			currentAuthToken = access_token;
			log.info("ClientCredentials access token updated " + currentAuthToken);
		} else {
			currentAuthToken = null;
			throw new Exception("API error recieved when fetching token " + response.toString());
		}

		
		return access_token;
	}

	/**
	 * FetchToken is an inner class that executes fetchToken() in different thread 
	 * and will update the shared auth token when it completes.
	 */
	class FetchToken extends TimerTask {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {

				String token = fetchToken();
				
			} catch (Exception e) {
				log.severe("Could not fetch auth token" +  e.getMessage());
				e.printStackTrace();
			}


		}

	}


	/**
	 * Command line call to fetch a token and print to standard out.
	 * 
	 * It will fetch a token using the values configured in AttConstants.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ClientCredentialsManager manager = new ClientCredentialsManager(AttConstants.HOST, 
				AttConstants.CLIENTIDSTRING, 
				AttConstants.CLIENTSECRETSTRING,
				AttConstants.CLIENTMODELSCOPE,AttConstants.CLIENTMODELREFRESHSECONDS, false);
		
		
		try {
			String token = manager.fetchToken();
			
			System.out.println("got Token " + token + " getCurrentToken " + manager.getCurrentToken());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	/**
	 * getCurrentToken returns the current valid Client Credentials token.
	 * if a valid token is not present then null will be returned.
	 * Callers should check for null before proceeding. 
	 * Fetch errors are logged and should be monitored for system failure.
	 */
	public String getCurrentToken() {
		// TODO Auto-generated method stub
		return currentAuthToken;
	}

}
