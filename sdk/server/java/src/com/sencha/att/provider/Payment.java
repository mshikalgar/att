package com.sencha.att.provider;

import java.util.logging.Logger;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.sencha.att.AttConstants;

public class Payment {
	
	private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);

		
	
	public static String TYPE_SUBSCRIPTION ="SUBSCRIPTION";

	public static String TYPE_SINGLEPAY ="SINGLEPAY";
	
	
	private String host;
	private String clientId;
	private String clientSecret;

	public Payment(String host, String clientId, String clientSecret) {
		    this.host = host;
		    this.clientId = clientId;
		    this.clientSecret = clientSecret;
		  
	}
	
	/**
	 * First calls notary to sign the payment request then forwards the request
	 * to either Subscription or Single pay depending on the type passed.
	 * Assuming both calls are successful then a JSON object with a adviceOfChargeUrl will be reutrned
	 * The user must be sent to this url to authorize the charge against their account.
	 * 
	 * @param type
	 * @param paymentDetails
	 * @return JSONObject with the {"adviceOfChargeUrl":https://someurl"} that the user needs to be redirected to.
	 */
	public JSONObject requestChargeAuth(String type, JSONObject paymentDetails) {
		
		
		JSONObject paycall = null;
		
		try {
			
			/*
			 *  We need to redirect the user back to our server after they have acce	
			 */
			paymentDetails.put("MerchantPaymentRedirectUrl",AttConstants.PAYMENTCOMPLETECALLBACK);
			
			NotaryProvider notary = new NotaryProvider(AttConstants.HOST, AttConstants.CLIENTIDSTRING, AttConstants.CLIENTSECRETSTRING);
			
			Payment payment = new Payment(AttConstants.HOST, AttConstants.CLIENTIDSTRING, AttConstants.CLIENTSECRETSTRING);
			
			TokenResponse response = notary.signPaylaod(paymentDetails);
			
			String signedPaymentDetail;
			
			signedPaymentDetail = response.getString("SignedDocument");
			String signature = response.getString("Signature");
			
			paycall = payment.chargeUser(type, signature, signedPaymentDetail);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return paycall;
		
		
	}
	
	
	public JSONObject chargeUser(String type, String signature, String signedPaymentDetail) {
		
		String url = host + "/Commerce/Payment/Rest/2/";
		
		if(TYPE_SUBSCRIPTION.equals(type)) {
			url += "Subscriptions";
		} else if(TYPE_SINGLEPAY.equals(type)) {
			url += "Transactions";
		}
		
		url += "?clientid=" + this.clientId + "&Signature=" + signature + "&SignedPaymentDetail=" + signedPaymentDetail;
		
		
		
		log.info("chargeUser" + url);
		
		String results = ApiRequestManager.get(url);
		JSONObject response = new JSONObject();
		
		try {
			response.put("adviceOfChargeUrl", results);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return response;
	
		
	}
	
	public JSONObject subscriptionDetails(String accessToken, String merchantSubscriptionId, String consumerId) {
		String url = host + "/Commerce/Payment/Rest/2/Subscriptions/" +merchantSubscriptionId+ "/Detail/" +consumerId + "?access_token=" + accessToken;
		
		log.info("subscriptionDetails: " + url);
		
		try {
			JSONObject results  = ApiRequestManager.getJson(url);
			return results;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject err = new JSONObject();
			try {
				err.put("error", e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return err;
		}
		
	}

	public JSONObject refundTransaction(String accessToken, String transactionId, JSONObject details) {
		String url = host + "/Commerce/Payment/Rest/2/Transactions/" +transactionId + "?access_token=" + accessToken;

		log.info("refundTransaction: " + url);
/*
 * 
 * {
"Action":"refund",
"RefundReasonCode":1,
"RefundReasonText":"Customer was not happy"
}
 * 
 * 
 * 
 */
		try {
			JSONObject results  = ApiRequestManager.putJson(url, details);
			return results;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject err = new JSONObject();
			try {
				err.put("error", e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return err;
		}
	}
	

	public JSONObject transactionStatus(String accessToken, String transactionAuthCode) {
		String url = host + "/Commerce/Payment/Rest/2/Transactions/TransactionAuthCode/" +transactionAuthCode + "?access_token=" + accessToken;

		log.info("refundTransaction: " + url);

		try {
			JSONObject results  = ApiRequestManager.getJson(url);
			return results;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject err = new JSONObject();
			try {
				err.put("error", e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return err;
		}
	}
	
	public JSONObject getNotification(String accessToken, String notificationId) {
		String url = host + "/Commerce/Payment/Rest/2/Notifications/" +notificationId + "?access_token=" + accessToken;

		log.info("getNotification: " + url);

		try {
			JSONObject results  = ApiRequestManager.getJson(url);
			return results;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject err = new JSONObject();
			try {
				err.put("error", e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return err;
		}
	} 
	
	public JSONObject acknowledgeNotification(String accessToken, String notificationId) {
		String url = host + "/Commerce/Payment/Rest/2/Notifications/" +notificationId + "?access_token=" + accessToken;

		log.info("getNotification: " + url);

		try {
			JSONObject results  = ApiRequestManager.putJson(url);
			return results;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject err = new JSONObject();
			try {
				err.put("error", e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return err;
		}
	} 
	
	
	

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		JSONTokener tk = new JSONTokener("{ \"Amount\":0.99, \"Category\":1, \"Channel\":\"MOBILE_WEB\", \"Description\":\"better than level 1\", \"MerchantTransactionId\":\"skuser2985trx20111029175423\", \"MerchantProductId\":\"level2\", \"MerchantPaymentRedirectUrl\":\"http://somewhere.com/PurchaseFulfillment\" }");
		
		try {
			JSONObject toSign = new JSONObject(tk);
			
			
			
			Payment payment = new Payment(AttConstants.HOST, AttConstants.CLIENTIDSTRING, AttConstants.CLIENTSECRETSTRING);
			
			JSONObject paycall = payment.requestChargeAuth(TYPE_SINGLEPAY, toSign);
			
			log.info("payments results " +  paycall.toString());
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
