package com.sencha.att.provider;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.json.JSONTokener;

import com.sencha.att.AttConstants;

/**
 * Use this class to make SMS API calls.
 * @author jason
 *
 */
public class ServiceProviderSMS {
  private String host = "";
  private String shortCode = "";
  private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);

  public ServiceProviderSMS(String host, String shortCode) {
    this.host = host;
    this.shortCode = shortCode;

  }

  private String getHost() {
    return host;
  }

  private String getShortCode() {
    return shortCode;
  }

  /****
   * This method requests information about the device
   *
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param message - The message
   * @return the JSON result
   */
  public TokenResponse sendSms(String access_token, String tel, String message) {
    return sendSms(getHost(), access_token, tel, message);
  }

  /****
   * This method requests information about the device
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param message - The message
   * @return the JSON result
   */
  public static TokenResponse sendSms(String host, String access_token, String tel, String message) {
    TokenResponse theReturn = null;
    URL url;


    if(AttConstants.STUBMODE) {
        String example = "{\"Id\" : \"SMSc04091ed284f5b4a\",\"ResourceReference\": {\"ResourceURL\": \"https://api-test.san1.attcompute.com/rest/sms/2/smessaging/outbox/SMSc04091ed284f5b4a\"}}";
         return  TokenResponse.getResponse(new JSONTokener(example));
    }


    try {
      url = new URL(host + "/rest/sms/2/messaging/outbox?access_token=" + access_token);
      log.info("url: " + url.toString());
      String[] telArray = tel.split(",");

      JSONObject object = new JSONObject();

      if (telArray.length == 1) {
        object.put("Address", "tel:" + telArray[0]);
      } else {
        for (int i = 0; i < telArray.length; i++) {
            telArray[i] = "tel:" + telArray[i];
        }
        object.put("Address", telArray);
      }

      object.put("Message", message);

      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Content-Type", "application/json");
      conn.setRequestProperty("Accept", "application/json");

      OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

      log.info("request: " + object.toString());
      object.write(wr);
      wr.flush();
      wr.close();

      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {

        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));

    } catch (Exception e) {
      log.info("exception" + e.getMessage());
      e.printStackTrace();
      theReturn =  TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }


  /****
   * This method requests the Sms Status
   *
   * @param access_token - The Token representing the logged in user
   * @param sms_id - The Sms Id
   * @return will return a JSONObject of the device information
   */
  public JSONObject smsStatus(String access_token, String sms_id){
    return smsStatus(getHost(), access_token, sms_id);
  }

  /****
   * This method requests the Sms Status
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param sms_id - The Sms Id
   * @return will return a JSONObject of the device information
   */
  public static TokenResponse smsStatus(String host, String access_token, String sms_id){
    TokenResponse theReturn = null;


    if(AttConstants.STUBMODE) {
        String example = "{\"DeliveryInfoList\": {\"DeliveryInfo\": [{  \"Id\" : \"msg0\", \"Address\" : \"5555555555\",\"DeliveryStatus\" : \"DeliveredToTerminal\"}] ,\"ResourceURL\": \"http://api-test.san1.attcompute.com:8080/rest/sms/2/messaging/outbox/SMSc04091ed284f5b4a\" }}";
         return  TokenResponse.getResponse(new JSONTokener(example));
    }

    URL url;
    try {
      url = new URL(host + "/rest/sms/2/messaging/outbox/" + sms_id + "?access_token=" + access_token);
      log.info("url: " + url.toString());
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoInput(true);
      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {

        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));
    } catch (Exception e) {
      theReturn = TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }

  /****
   * This method requests the Sms Status
   *
   * @param token - The Token representing the logged in user
   * @param shortcode - The short code
   * @return will return a JSONObject of the device information
   */
  public JSONObject receiveSms(String token){
    return receiveSms(getHost(), token, getShortCode());
  }

  /****
   * This method requests the Sms Status
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param shortcode - The short code
   * @return will return a JSONObject of the device information
   */
  public static TokenResponse receiveSms(String host, String access_token, String shortcode){
    TokenResponse theReturn = null;
    URL url;
    try {
      url = new URL(host + "/rest/sms/2/messaging/inbox?access_token=" + access_token + "&RegistrationID=" + shortcode);
      log.info("url: " + url.toString());
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoInput(true);
      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {

        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));

    } catch (Exception e) {
      theReturn = TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }

}
