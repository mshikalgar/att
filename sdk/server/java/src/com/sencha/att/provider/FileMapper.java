package com.sencha.att.provider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


/**
 * FileMapper a simple class that is used by ServiceProviderMMS to map 
 * api requests to actual File references.
 * 
 * The FileManager is needed to work around two realities of modern mobile web applications:
 * 
 * 1) Many mobile devices do not support file uploads from the web browser.  So the user will need some
 * other way to specify the file they want to upload.
 *   
 * 2) For the devices that do support it or for desktop browsers, in the case of ajax applications
 *    files are uploaded via a independent request: hidden iframe, html5 file upload, flash etc
 *    once the file has been uploaded and verified back to the browser then an ajax request
 *    is sent back to the server to initiate the actual request with the form data. 
 *    In this case to send the MMS.
 * 
 * See ServiceProviderMMS for more details.
 * 
 * The default implementation will take a string and assume its a valid file path and create a new
 * file reference from it.  Subclass this class and provide your own impl of getFileForReference
 * 
 * @author jason
 *
 */
public class FileMapper {
	
	
	public class FileMapping {
		public String fileName;
		public String fileType;
		public InputStream stream;

		public FileMapping(String fileName, String fileType, InputStream stream) {
			this.fileName = fileName;
			this.fileType = fileType;
			this.stream = stream;
			
		}
		
	}
	
	/**
	 * Creates a valid File reference based on the string passed.
	 * 
	 * @param fileRefrence
	 * @return File
	 * @throws FileNotFoundException 
	 */
	public FileMapping getFileForReference(String fileRefrence) throws FileNotFoundException {
		  File file = new File(fileRefrence);
		  InputStream stream = new FileInputStream(file);
	      String type = "image/jpeg";
		return new FileMapping(file.getName(),type,stream );
	}
	


}
