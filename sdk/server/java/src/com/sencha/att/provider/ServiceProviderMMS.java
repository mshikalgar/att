package com.sencha.att.provider;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64InputStream;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.sencha.att.AttConstants;


/**
 *  Use this class to make MMS api calls.
 *
 */
public class ServiceProviderMMS {
  public static final String SEPERATOR = "--";
  private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);

  private String host = "";

  public ServiceProviderMMS(String host) {
    this.host = host;
  }

  private String getHost() {
    return host;
  }

  /****
   * This method sends a MMS Message
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param fileName - The name of the file
   * @param stream - The Stream of the data object
   * @param fileType - type of file
   * @return the JSON result
   */
  public TokenResponse sendMms(String access_token, String tel, String fileType, String fileName, InputStream stream, String subject, String priority) {
    return sendMms(getHost(), access_token, tel, fileType, fileName, stream, subject, priority);

  }


  /****
   * This method sends a MMS Message
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param fileName - The name of the file
   * @param stream - The Stream of the data object
   * @param fileType - type of file
   * @return the JSON result
   */
  public static TokenResponse sendMms(String host, String access_token, String tel, String fileType, String fileName,
      InputStream stream, String subject,  String priority) {
    TokenResponse theReturn = null;

    log.info("send MMS " +  tel + " " + fileType + " " + fileName);

    if(AttConstants.STUBMODE) {
        String example = "{\"Id\" : \"MMSc0401e2388f922aa\",\"ResourceReference\": {\"ResourceURL\": \"https://api-test.san1.attcompute.com/rest/mms/2/messaging/outbox/MMSc0401e2388f922aa\"}}";
         return  TokenResponse.getResponse(new JSONTokener(example));
    }

    URL url;
    try {
      url = new URL(host + "/rest/mms/2/messaging/outbox?access_token=" + access_token);
      log.info("url: " + url.toString());
      String boundry = getBoundry();

      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setRequestMethod("POST");
      conn.setRequestProperty("content-type", "application/json");
      conn.setRequestProperty("accept", "application/json");
      conn.setRequestProperty("content-type",
          "multipart/form-data; type=\"application/json\"; start=\"<part0@sencha.com>\"; boundary=\"" + boundry + "\"");

      OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
      wr.write(buildBeginMMSMessage(boundry, tel, fileName, fileType, subject, priority));
      Base64InputStream b64is = new Base64InputStream(stream, true, -1, null);
      BufferedReader reader = new BufferedReader(new InputStreamReader(b64is));
      String thisLine;
      while ((thisLine = reader.readLine()) != null) {
        wr.write(thisLine);
        wr.write("\n");
      }
      wr.write(buildEndMMSMessage(boundry));
      wr.flush();
      wr.close();

      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {

        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));
    } catch (Exception e) {
      theReturn = TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }

  /****
   * This method gets the MMS Status
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param mms_id - The MMS id
   * @return the JSON result
   */
  public TokenResponse mmsStatus(String access_token, String mms_id){
    return mmsStatus(getHost(), access_token, mms_id);
  }

  /****
   * This method gets the MMS Status
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param mms_id - The MMS id
   * @return the JSON result
   */
  public static TokenResponse mmsStatus(String host, String access_token, String mms_id){
    TokenResponse theReturn = null;
    URL url;


    if(AttConstants.STUBMODE) {
        String example = "{\"DeliveryInfoList\": {\"DeliveryInfo\": [{  \"Id\" : \"msg0\", \"Address\" : \"tel:5555555555\", \"DeliveryStatus\" : \"DeliveredToTerminal\"}] ,\"ResourceURL\": \"http://api-test.san1.attcompute.com:8080/rest/mms/2/messaging/outbox/MMSc0401e2388f922ad\" }}";
         return  TokenResponse.getResponse(new JSONTokener(example));
    }




    try {
      url = new URL(host + "/rest/mms/2/messaging/outbox/" + mms_id + "?access_token=" + access_token);
      log.info("url: " + url.toString());

      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(false);
      conn.setDoInput(true);
      conn.setRequestMethod("GET");

      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {

        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));
    } catch (Exception e) {
      theReturn = TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }

  private static String buildBeginMMSMessage(String boundry, String tel, String fileName, String type, String subject, String priority) {

    String[] telArray = tel.split(",");

    JSONObject object = new JSONObject();

    if (telArray.length == 1) {
        tel = "\"tel:" + telArray[0] + "\"";
    } else {
      for (int i = 0; i < telArray.length; i++) {
          telArray[i] = "\"tel:" + telArray[i] + "\"";
      }
      tel = "[" + combine(telArray, ",") + "]";
    }

    log.info("tel =  " + tel);

    StringBuffer theReturn = new StringBuffer("--").append(boundry).append("\n");
    theReturn.append("Content-Type: application/json").append("\n");
    theReturn.append("Content-ID: <part0@sencha.com>").append("\n");
    theReturn.append("Content-Disposition: form-data; name=\"root-fields\"").append("\n");
    theReturn.append("\n");
    theReturn.append("{ \"Address\" : ").append(tel).append(
        ", \"Subject\" : \"" + subject + "\", \"Priority\": \"" + priority + "\" }").append("\n");
    theReturn.append("--").append(boundry).append("\n");
    theReturn.append("Content-Type: ").append(type).append("\n");
    theReturn.append("Content-ID: <").append(fileName).append(">").append("\n");
    theReturn.append("Content-Transfer-Encoding: base64").append("\n");
    theReturn.append("Content-Disposition: attachment; name=\"").append(fileName).append("\"").append("\n");
    theReturn.append("\n");

    log.info("MMS Message = " + theReturn.toString());
    return theReturn.toString();
  }

  private static String buildEndMMSMessage(String boundry) {
    StringBuffer theReturn = new StringBuffer();
    theReturn.append("\n");
    theReturn.append("--").append(boundry).append("--");
    return theReturn.toString();
  }

  private static String getBoundry() {
    return "----=_Part_0_1" + Math.round((Math.random() * 10000000)) + "." + new Date().getTime() * 1000;

  }

  private static String combine(String[] s, String glue){
      int k=s.length;
      if (k==0)
        return null;
      StringBuilder out=new StringBuilder();
      out.append(s[0]);
      for (int x=1;x<k;++x)
        out.append(glue).append(s[x]);
      return out.toString();
  }


}
