package com.sencha.att.provider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

import org.json.JSONTokener;

import com.sencha.att.AttConstants;


/**
 * This class handles the low-level oauth calls including getting the url of the oauth login and fetching the auth token after the user has completed the oauth login sequence.
 * @author jason
 *
 */
public class ServiceProviderOauth {
  private String host = "";
  private String client_id = "";
  private String client_secret = "";
  private String callback = "";
  private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);

  /******
   *
   * @param host - The domain name (or ip address) and port the request is
   * @param client_id The clients id. This may look something like
   *          c4c9084c6e9cb6ca01886a15e7dfd486
   * @param client_secret
   * @param callback The URL to callback to when the user has logged in. Example
   *          http://localhost:8080
   *
   */
  public ServiceProviderOauth(String host, String client_id, String client_secret, String callback) {
    this.host = host;
    this.client_id = client_id;
    this.client_secret = client_secret;
    this.callback = callback;
System.out.println("Callback " + this.callback);
  }

  private String getHost() {
    return host;
  }

  private String getClientID() {
    return client_id;
  }

  private String getClientSecret() {
    return client_secret;
  }

  private String getCallback() {
    return callback;
  }




  /****
   * Generates a Authorization URL to login the user
   *
   * @param scope - The scope that will be embedded in the request. Example
   *          PAYMENT
   * @return will return a url where the user can log in. Example
   *         https://beta-api.att.com/oauth/authorize?scope=PAYMENT&client_id=
   *         c4c9084c6e9cb6ca01886a15e7dfd486
   *         &redirect_uri=http://localhost:8080/att/callback
   */
  public String oauthUrl(String scope) {
    return oauthUrl(getHost(), scope, getClientID(), getCallback());
  }

  /****
   * Generates a Authorization URL to login the user
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param scope - The scope that will be embedded in the request. Example
   *          PAYMENT
   * @param client_id - The clients id. This may look something like
   *          c4c9084c6e9cb6ca01886a15e7dfd486
   * @param callback - The URL to callback to when the user has logged in.
   *          Example http://localhost:8080
   * @return will return a url where the user can log in. Example
   *         https://beta-api.att.com/oauth/authorize?scope=PAYMENT&client_id=
   *         c4c9084c6e9cb6ca01886a15e7dfd486
   *         &redirect_uri=http://localhost:8080/att/callback
   */
  public static String oauthUrl(String host, String scope, String client_id, String callback) {

String url =  host + "/oauth/authorize?scope=" + scope + "&client_id=" + client_id + "&redirect_uri="
+ callback;
      System.out.println("Oauth redirect " +url);

    return url;
  }

  /****
   *
   * @param clientID - The clients id. This may look something like
   *          c4c9084c6e9cb6ca01886a15e7dfd486
   * @param clientSecret - The clients secret. This may look something like
   *          a0725b93040cfe0e
   * @param code - This is the one time code returned when the user has logged
   *          in.
   * @return returns a TokenResponse which should concern a access_token
   * @throws ServiceProviderException - throws if any problems occur
   */
  public TokenResponse getToken(String code) {
    return getToken(getHost(), getClientID(), getClientSecret(), code);

  }

  /****
   *
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param client_id - The clients id. This may look something like
   *          c4c9084c6e9cb6ca01886a15e7dfd486
   * @param client_secret - The clients secret. This may look something like
   *          a0725b93040cfe0e
   * @param code - This is the one time code returned when the user has logged
   *          in.
   * @return returns a TokenResponse which should concern a access_token
   * @throws ServiceProviderException - throws if any problems occur
   */
  public static TokenResponse getToken(String host, String client_id, String client_secret, String code) {
    TokenResponse theReturn = null;
    URL url;
    try {
      url = new URL(host + "/oauth/access_token?client_id=" + client_id + "&client_secret=" + client_secret + "&code="
          + code+"&grant_type=authorization_code");
      String responseStr = fetchContent(url);

      log.info("getToken raw response" + responseStr);

      theReturn = TokenResponse.getResponse(new JSONTokener(responseStr));

      log.info("theReturn: " + theReturn.toString(2));

    } catch (Exception e) {
    log.warning("getToken failed "  +  e.getMessage());
    e.printStackTrace();
      theReturn = TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }

private static String fetchContent(URL url) throws IOException {
    log.info("url: " + url.toString());
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoInput(true);
      StringBuffer response = new StringBuffer();

      log.info("conn.getResponseCode(): " +conn.getResponseCode()  +  " " + conn.getContentLength() +  " " + conn.getContentType());


      if (conn.getResponseCode() < 400) {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        log.info("reading from buffer: ");

        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));


        log.info("reading from buffer error ");

        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());

      String responseStr = response.toString();
    return responseStr;
}
}
