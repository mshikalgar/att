package com.sencha.att.provider;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.logging.Logger;

import org.json.JSONTokener;

import com.sencha.att.AttConstants;


/**
 * ServiceProvider provides implementations of Device Info, Device Location, and wapPush
 *
 */
public class ServiceProvider {
  private String host = "";
  private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);

  public ServiceProvider(String host) {
    this.host = host;
  }

  private String getHost() {
    return host;
  }

  /****
   * This method requests information about the device
   * 
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @return will return a JSONObject of the device information
   */
  public TokenResponse deviceInfo(String access_token, String tel) {
    return deviceInfo(getHost(), access_token, tel);
  }

  /****
   * This method requests information about the device
   * 
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @return will return a JSONObject of the device information
   */
  public static TokenResponse deviceInfo(String host, String access_token, String tel) {
    TokenResponse theReturn = null;
    
    if(AttConstants.STUBMODE) {
    	String example = "{ \"deviceId\":  { \"acwdevcert\" : \"UNKNOWN\", \"acwrel\" : \"UNKNOWN\", \"acwmodel\" : \"iPhone_3G\", \"acwvendor\" : \"APL\" },\"capabilities\" : { \"acwav\" : \"Y\", \"acwaocr\": \"Y\", \"acwcf\": \"MED\" , \"acwtermtype\": \"N\" }}";
  	   return  TokenResponse.getResponse(new JSONTokener(example));
    }
    
    URL url;
    try {
      url = new URL(host + "/1/devices/tel:" + tel + "/info?access_token=" + access_token);
      log.info("url: " + url.toString());
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoInput(true);
      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));
    } catch (Exception e) {
      TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }

  /****
   * This method requests information about the device
   * 
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param accuracy - The accuracy of the telephone
   * @return will return a JSONObject of the device location
   */
  public TokenResponse deviceLocation(String access_token, String tel, int accuracy, int acceptableAccuracy, String tolerance){
    return deviceLocation(getHost(), access_token, tel, accuracy, acceptableAccuracy, tolerance);
  }

  /****
   * This method requests information about the device
   * 
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param accuracy - The accuracy of the telephone
 * @param string2 
 * @param string 
   * @return will return a JSONObject of the device location
   */
  public static TokenResponse deviceLocation(String host, String access_token, String tel, int accuracy, int acceptableAccuracy, String tolerance) {
    TokenResponse theReturn = null;
   
    
    
    URL url;
    try {
    	
      String tmp = host + "/1/devices/tel:" + tel + "/location?access_token=" + access_token;
      
      if (accuracy > 0) {
    	  tmp += "&requestedAccuracy=" + accuracy;
      }
      

      if (acceptableAccuracy > 0) {
    	  tmp += "&acceptableAccuracy=" + acceptableAccuracy;
      }
      
      
      if (null != tolerance && tolerance.trim().length() > 0) {
    	  tmp += "&tolerance=" + tolerance;
      }
      
      
      url = new URL(tmp);
      log.info("url: " + url.toString());
      
      
      if(AttConstants.STUBMODE) {
      	String example = "{\"accuracy\": \"320\",\"latitude\": \"47.1667\",\"longitude\": \"-122.1222\",\"timestamp\": \"2011-11-22T05:53:27.000-08:00\"}";
    	   return  TokenResponse.getResponse(new JSONTokener(example));
      }
      
      
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoInput(true);
      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));
    } catch (Exception e) {
      theReturn = TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }

  /****
   * This method pushes a WAP Message
   * 
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param xml - The message to push
   * @return will return a JSONObject of the device location
   */
  public TokenResponse wapPush(String access_token, String tel, String xml, String subject, String priority) {
    return wapPush(getHost(), access_token, tel, xml, subject, priority);
  }

  /****
   * This method pushes a WAP Message
   * 
   * @param host - The domain name (or ip address) and port the request is
   *          submitted to. Example https://beta-api.att.com
   * @param access_token - The Token representing the logged in user
   * @param tel - The Telephone number of the user
   * @param xml - The message to push
 * @param priority 
 * @param subject 
   * @return will return a JSONObject of the device location
   */
  public static TokenResponse wapPush(String host, String access_token, String tel, String xml, String subject, String priority) {
	  
	if(AttConstants.STUBMODE) {
	  String example =  "{\"id\" : \"WAPPUSHc03c32ae55bcf47d\"}";
	  return  TokenResponse.getResponse(new JSONTokener(example));
	}
	  
	  
	 
	  
    TokenResponse theReturn = null;
    URL url;
    try {
      url = new URL(host + "/1/messages/outbox/wapPush?access_token=" + access_token);
      log.info("url: " + url.toString());
      String boundry = getBoundry();

      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setRequestMethod("POST");
      conn.setRequestProperty("content-type", "application/json");
      conn.setRequestProperty("accept", "application/json");
      conn.setRequestProperty("content-type",
          "multipart/form-data; type=\"application/json\"; start=\"<part0@sencha.com>\"; boundary=\"" + boundry + "\"");

      OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
      log.info("request: " + buildWAPMessage(boundry, tel, xml, subject, priority));
      wr.write(buildWAPMessage(boundry, tel, xml, subject, priority));
      wr.flush();
      wr.close();
      StringBuffer response = new StringBuffer();
      if (conn.getResponseCode() < 400) {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      } else {
        BufferedReader is = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        String str;
        while (null != ((str = is.readLine()))) {
          response.append(str);
        }
        is.close();
      }
      log.info("response: " + response.toString());
      theReturn = TokenResponse.getResponse(new JSONTokener(response.toString()));
    } catch (Exception e) {
      TokenResponse.getResponse(e.getMessage());
    }
    return theReturn;
  }

  private static String buildWAPMessage(String boundry, String telephone, String message, String subject, String priority) {
    StringBuffer theReturn = new StringBuffer("--").append(boundry).append("\n");
    theReturn.append("Content-Type: text/xml").append("\n");
    theReturn.append("Content-ID: <part0@sencha.com>").append("\n");
    theReturn.append("Content-Disposition: form-data; name=\"root-fields\"").append("\n");
    theReturn.append("\n");
    theReturn.append("<wapPushRequest>").append("\n");
    theReturn.append("<addresses>").append("\n");
    theReturn.append("<address>tel:").append(telephone).append("</address>").append("\n");
    theReturn.append("</addresses>").append("\n");
    theReturn.append("<subject>").append(subject).append("</subject>").append("\n");
    theReturn.append("<priority>").append(priority).append("</priority>").append("\n");
    theReturn.append("</wapPushRequest>").append("\n");
    theReturn.append("--").append(boundry).append("\n");
    theReturn.append("Content-Type: text/xml").append("\n");
    theReturn.append("Content-ID: <part2@sencha.com>").append("\n");
    theReturn.append("\n");
    theReturn.append("Content-Disposition: form-data; name=\"PushContent\"").append("\n");
    theReturn.append("Content-Type: text/vnd.wap.si").append("\n");
    theReturn.append("Content-Length: 12").append("\n");
    theReturn.append("X-Wap-Application-Id: x-wap-application:wml.ua").append("\n");
    theReturn.append("\n");
    theReturn.append(message);
    theReturn.append("\n");
    theReturn.append("--").append(boundry).append("--");
    return theReturn.toString();
  }

  private static String getBoundry() {
    return "----=_Part_0_1" + Math.round((Math.random() * 10000000)) + "." + new Date().getTime() * 1000;

  }
}
