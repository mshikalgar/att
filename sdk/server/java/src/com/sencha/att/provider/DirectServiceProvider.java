package com.sencha.att.provider;

import java.io.File;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sencha.att.AttConstants;
import com.sencha.att.provider.FileMapper.FileMapping;


/**
 * DirectServiceProvider is the glue code between the AttDirectRouterServlet and the Service methods.
 * 
 * when a request is made to the AttDirectRouterServlet it will lookup the method in this class.
 * 
 * The methods in this class will make the call to the appropriate service provider Implementation.
 * 
 * @author jason
 *
 */
public class DirectServiceProvider {
	
	private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);

	
	private static FileMapper fileMapper = null;
	
	
	static {
		init();
	}
	

	/**
	 * Loads the FileMapper specified in the att config file for MMS
	 */
	private static void init() {
	
		ClassLoader classLoader = DirectServiceProvider.class.getClassLoader();
if(AttConstants.FILEMAPPERCLASSNAME != null) {
	    try {
	        Class aClass = classLoader.loadClass(AttConstants.FILEMAPPERCLASSNAME);
	        log.info("Using FileMapper " + aClass.getCanonicalName());
	       
	        fileMapper = (FileMapper) aClass.newInstance();
	        
	    } catch (ClassNotFoundException e) {
	        e.printStackTrace();
	    } catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

if(fileMapper == null ) {
	fileMapper = new FileMapper();
}


	}
	
	
  /****
   * Forwards request to ServiceProvider.getOAuthURL
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getOAuthURL
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getoauthurl(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProviderOauth.oauthUrl(request.getString(ServiceProviderConstants.HOST), array.getString(0),
              request.getString(ServiceProviderConstants.CLIENTID),
              request.getString(ServiceProviderConstants.CALLBACK)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;

  }


/****
   * Forwards request to ServiceProvider.getDeviceInfo
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getDeviceInfo
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getdeviceinfo(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProvider.deviceInfo(request.getString(ServiceProviderConstants.HOST),
              request.getString(ServiceProviderConstants.TOKEN), array.getString(0)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;
  }

  /****
   * Forwards request to ServiceProvider.getDeviceInfo
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getDeviceInfo
   * @return will return a JSON Object with the device info
   */
  public static JSONObject ServiceProviderMMS(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProvider.deviceInfo(request.getString(ServiceProviderConstants.HOST),
              request.getString(ServiceProviderConstants.TOKEN), array.getString(0)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;
  }

  /****
   * Forwards request to ServiceProvider.getDeviceLocation
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getDeviceLocation
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getdevicelocation(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      
      log.info("args" + array.toString(2));
      
      
      int accuracy = -1;
      int acceptableAccuracy = -1;
      String tolerance = "";
      
      if(array.length() >= 2) {
    	  accuracy = array.getInt(1);
      }
      
      if(array.length() >= 3) {
    	  acceptableAccuracy = array.getInt(2);
      }
      
      if(array.length() >= 4) {
    	  tolerance = array.getString(3);
      }
      
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProvider.deviceLocation(request.getString(ServiceProviderConstants.HOST),
              request.getString(ServiceProviderConstants.TOKEN), array.getString(0), accuracy, acceptableAccuracy, tolerance));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;
  }

  /****
   * Forwards request to ServiceProvider.sendSms
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to sendSms
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getsendsms(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProviderSMS.sendSms(request.getString(ServiceProviderConstants.HOST),
              request.getString(ServiceProviderConstants.TOKEN), array.getString(0), array.getString(1)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;
  }



  /****
   * Forwards request to ServiceProvider.getSmsStatus
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getSmsStatus
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getsmsstatus(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProviderSMS.smsStatus(request.getString(ServiceProviderConstants.HOST),
              request.getString(ServiceProviderConstants.TOKEN), array.getString(0)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;
  }

  /****
   * Forwards request to ServiceProvider.receiveSms
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to receiveSms
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getreceivesms(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProviderSMS.receiveSms(request.getString(ServiceProviderConstants.HOST),
              request.getString(ServiceProviderConstants.TOKEN), request.getString(ServiceProviderConstants.SHORTCODE)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;
  }

  /****
   * Forwards request to ServiceProvider.mmsStatus
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to mmsStatus
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getsendmms(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      
      log.info("array " + array.toString(2));
      
      
      FileMapping file = fileMapper.getFileForReference(array.getString(1));
      
      
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProviderMMS.sendMms(request.getString(ServiceProviderConstants.HOST),
              request.getString(ServiceProviderConstants.TOKEN), array.getString(0), file.fileName, file.fileType, file.stream, array.getString(2), array.getString(3)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;
  }
  
  /****
   * Forwards request to ServiceProvider.mmsStatus
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to mmsStatus
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getmmsstatus(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      theReturn.put(
          ServiceProviderConstants.RESULT,
          ServiceProviderMMS.mmsStatus(request.getString(ServiceProviderConstants.HOST),
              request.getString(ServiceProviderConstants.TOKEN), array.getString(0)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;
  }
  
  

  /****
   * Forwards request to ServiceProvider.receiveSms
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to receiveSms
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getwappush(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
    	JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
        log.info(array.toString(2));
        theReturn.put(
            ServiceProviderConstants.RESULT,
            ServiceProvider.wapPush(request.getString(ServiceProviderConstants.HOST),
                request.getString(ServiceProviderConstants.TOKEN),array.getString(0), array.getString(1), array.getString(2), array.getString(3)));
      } catch (Exception e) {
        try {
          theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
        } catch (JSONException e1) {
          System.out.println(e1.getMessage());
          e1.printStackTrace();
        }
      }
      return theReturn;
  }
  
  
  
  /****
   * Forwards request to ServiceProvider.getOAuthURL
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getOAuthURL
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getrequestchargeauth(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      
      Payment payment = new Payment(AttConstants.CLIENTSECRETSTRING, AttConstants.CLIENTSECRETSTRING, AttConstants.CLIENTSECRETSTRING);
      
      theReturn.put(
          ServiceProviderConstants.RESULT,
          payment.requestChargeAuth(array.getString(0), array.getJSONObject(1)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;

  }
  
  

  
  /****
   * Forwards request to ServiceProvider.getOAuthURL
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getOAuthURL
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getsubscriptiondetails(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      
      Payment payment = new Payment(AttConstants.HOST, AttConstants.CLIENTSECRETSTRING, AttConstants.CLIENTSECRETSTRING);
      
      theReturn.put(
          ServiceProviderConstants.RESULT,
          payment.subscriptionDetails(request.getString(ServiceProviderConstants.TOKEN), array.getString(0),array.getString(1)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;

  }
  
  
  /****
   * Forwards request to ServiceProvider.getOAuthURL
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getOAuthURL
   * @return will return a JSON Object with the device info
   */
  public static JSONObject getrefundtransaction(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      
      Payment payment = new Payment(AttConstants.HOST, AttConstants.CLIENTSECRETSTRING, AttConstants.CLIENTSECRETSTRING);
      
      theReturn.put(
          ServiceProviderConstants.RESULT,
          payment.refundTransaction(request.getString(ServiceProviderConstants.TOKEN), array.getString(0),  array.getJSONObject(1)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;

  }
  

  /****
   * Forwards request to ServiceProvider.getOAuthURL
   * 
   * @param request - This method unpacks the JSONObject and forwards the request to getOAuthURL
   * @return will return a JSON Object with the device info
   */
  public static JSONObject  gettransactionstatus(JSONObject request) {
    JSONObject theReturn = new JSONObject();
    try {
      JSONArray array = request.getJSONArray(ServiceProviderConstants.DATA);
      
      Payment payment = new Payment(AttConstants.HOST, AttConstants.CLIENTSECRETSTRING, AttConstants.CLIENTSECRETSTRING);
      
      theReturn.put(
          ServiceProviderConstants.RESULT,
          payment.transactionStatus(request.getString(ServiceProviderConstants.TOKEN), array.getString(0)));
    } catch (Exception e) {
      try {
        theReturn.put(ServiceProviderConstants.ERROR, e.getMessage());
      } catch (JSONException e1) {
        System.out.println(e1.getMessage());
        e1.printStackTrace();
      }
    }
    return theReturn;

  }
  
}
