package com.sencha.att;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Properties;
import java.util.ResourceBundle;



/**
 * AttConstants provides a central location for application configuration and other constants throughout the application.
 *  Via a static init() method AttConstants will attempt to load att-api.properties  which contains the att keys for your application.
 *  Please see the main SDK documentation  for details on how to configure att-api.properties.
 *
 *  AttConstants.CLIENTIDSTRING
 *  AttConstants.CLIENTSECRETSTRING
 *  AttConstants.SHORTCODE
 *  AttConstants.HOST and others are populated from the values found in att-api.properties
 *
 *  The SDK server code uses these constants when making api calls to ATT.
 *
 * To provide some flexibility in the deployment of the application there are two ways to load the properties file. The default is to look on the classpath using the classloader for att-api.properties You can override this behavior by specifying a java system property. By specifying a system property the application can be re-configured without recompiling the war file.
 *
 *  com.sencha.example.servlet.AttConstants will look first for a system property att.api.conf
 *
 * String apiFile = System.getProperty("att.api.conf");
 * If it finds att.api.conf it will assume that its value is the full path to the att config file.
 *
 * if att.api.conf is not found, then it will attempt to load the file using the classloader:
 *
 * Thread.currentThread().getContextClassLoader().getResourceAsStream("att-api.properties");
 * If neither of these methods will meet your deployment needs you will need to modify AttConstants to load your configuration files from an alternate location.
 *
 *
 *
 */
public class AttConstants {



    /**
     * CLIENTIDSTRING the api key of the application provisioned with ATT
     */
    public static String CLIENTIDSTRING;


    /**
     * CLIENTSECRETSTRING the api secret key of the application provisioned with ATT
     */
    public static  String CLIENTSECRETSTRING;

    /**
     * CLIENTSECRETSTRING the sms short code and RegistrationID of the application provisioned with ATT
     */
    public static  String SHORTCODE;

    /**
     * enables debugging output
     */
    public static final boolean DEBUG = true;

    /**
     * The https url where the ATT api resides.
     */
    public static  String HOST;


    /**
     * The url of this application that ATT will return the user to when the user completes their auth flow.
     */
    public static  String CALLBACK_SERVER;



    /**
     * STUBMODE is used for offline debugging.  Fake data is returned for all api calls.
     */
    public static boolean STUBMODE = false;

    /**
     * The list of scopes that the application wants to gain access to when making API calls that use Autonomous  Client.
     */
    public static String CLIENTMODELSCOPE;

    /**
     * The number of Seconds in between fetches of new auth tokens for Autonomous  Client
     */
    public static int CLIENTMODELREFRESHSECONDS;

    /**
     * The fully qualified class name of the FileMapper used by MMS to send files.
     */
    public static String FILEMAPPERCLASSNAME;


    public static String PAYMENTCOMPLETECALLBACK;

    static {
        init();
    }

    private static void init() {

        String apiFile = System.getProperty("att.api.conf");

        Properties properties = new Properties();

        InputStream inputStream  = null;

        if(apiFile != null) {
            System.out.println("using api config " + apiFile);
            try {
                inputStream = new FileInputStream(new File(apiFile));
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("att-api.properties");

        }

        try {
            properties.load(inputStream);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        ///CLIENTIDSTRING = bundle.getString("apiKey");
        //CLIENTSECRETSTRING = bundle.getString("secretKey");
        //SHORTCODE = bundle.getString("shortCode");
        //HOST= bundle.getString("apiHost");
        //CALLBACK_SERVER= bundle.getString("authCallbackUrl");


        CLIENTIDSTRING = properties.getProperty("apiKey");
        CLIENTSECRETSTRING = properties.getProperty("secretKey");
        SHORTCODE = properties.getProperty("shortCode");
        HOST= properties.getProperty("apiHost");
        CALLBACK_SERVER= properties.getProperty("authCallbackUrl");

        CLIENTMODELSCOPE= properties.getProperty("clientModelScope");
        CLIENTMODELREFRESHSECONDS = Integer.parseInt(properties.getProperty("clientModelRefreshSeconds"));


        FILEMAPPERCLASSNAME = properties.getProperty("fileMapperClassName");



        PAYMENTCOMPLETECALLBACK = properties.getProperty("paymentCompleteCallbackUrl");

        String temp = properties.getProperty("useStubs");
        STUBMODE = "true".equals(temp);
        if(STUBMODE) {
            System.out.println("WARNING! Using API stubs.  All responses will be fake. (set useStubs to false in att-api.properites) ");
        }

    }


    public static final String SCOPE = "scope";
    public static final String CODE = "code";
    public static final String PAYMENTSCOPE = "PAYMENT";
    public static final String REDIRECT = "redirect";

    public static final String ISAUTHORIZED = "isAuthorized";

    public static final String STARTHTML = "/index.html";
    public static final String REDIRECTHTML = "/redirect.html";
    public static final String ERRORHTML = "/error.html    ";
    public static final String CALLBACKHTML = "/callback.html";
    public static final String AUTHCHECKSTRING = "/auth/check";
    public static final String AUTHURLSTRING = "/auth/url";
    public static final String PAYMENTID = "payment_id";
    public static final String SUBSCRIPTIONID = "subscription_id";

    public static final String PROVIDER = "ServiceProvider";


    public static final String TID = "tid";
    public static final String TYPE = "type";
    public static final String ACTION = "action";
    public static final String METHOD = "method";
    public static final String TOKEN = "token";
    public static final String PAYMENTKEY = "paymentKey";
    public static final String ERROR = "error";
    public static final String ID = "id";
    public static final String SUCCESS = "success";
    public static final String TRXID = "trxID";

    public static final String PAYMENTERROR = "Payment Error";
    public static final String RPC = "rpc";
    public static final String EXCEPTION = "exception";



    public static final String REDIRECT_HTML_PRE = "<!DOCTYPE html><html><head><script>window.parent.postMessage('";

    public static final String REDIRECT_HTML_POST = "', '*');</script></head><body></body></html>"; //{"success":true, "msg" : "Authorized!"}


	public static final String TransactionAuthCode = "TransactionAuthCode";





}
