package com.sencha.att.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.util.log.Log;
import org.json.JSONException;
import org.json.JSONObject;

import com.sencha.att.AttConstants;
import com.sencha.att.provider.ClientCredentialsManager;
import com.sencha.att.provider.DirectServiceProvider;
import com.sencha.att.provider.ServiceProviderConstants;
import com.sencha.att.provider.TokenResponse;

/**
 * This method passes white listed methods through to the Provider instance
 */
public class AttDirectRouterServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  

  public static final String[] whiteList = {"oauthUrl"};
  
  
  /**
   * API methods which use the client credentials auth token.
   */
  public static final String[] clientCredentialsMethods = {"sendSms", "smsStatus", "receiveSms", "sendMms", "mmsStatus", "wapPush", "requestChargeAuth", "subscriptionDetails", "refundTransaction", "transactionStatus"};

 /**
  * The servlets instance of the ClientCredentialsManager configured using ATTConstatnts. 
  */
  private ClientCredentialsManager credentialsManager;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public AttDirectRouterServlet() {
    super();
  }

  public void init() throws ServletException {

    if(AttConstants.DEBUG){
      log("AT&T Provider initialized.");
      log("");
      log("API endpoint:  " + AttConstants.HOST);
      log("Client ID:     " + getClientID());
      log("Client Secret: " + getClientSecret());
      log("Shortcode:     " + AttConstants.SHORTCODE);
    }
    
    
    this.credentialsManager = new ClientCredentialsManager(AttConstants.HOST, 
			AttConstants.CLIENTIDSTRING, 
			AttConstants.CLIENTSECRETSTRING,
			AttConstants.CLIENTMODELSCOPE,AttConstants.CLIENTMODELREFRESHSECONDS, true);
    
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doPost(request, response);
  }

  /**
   * All HTTP methods are routed through doPost. 
   * 
   * RPC calls to api methods are validated against the authentication method required for that method.
   * A users auth token is fetched from the session if it is required for the quest
   * if the request is for an autonomous client method then the credentialsManager is called to fetch that 
   * auth token
   * 
   * if a valid auth token is not found then then an api error is returned to the caller
   * if the method is not a recognized method then an api error is returned to the caller.
   * 
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   *      response)
   */

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Writer out = response.getWriter();
    try {

      JSONObject requestJSON = getData(request);
      JSONObject responseJSON = new JSONObject();

      String action = requestJSON.getString(AttConstants.ACTION);
      String method = requestJSON.getString(AttConstants.METHOD);

      boolean authorized = false;

      String token = (String) request.getSession().getAttribute(AttConstants.TOKEN);
      
      
      log("Using access token  " + token + " method " + method);
      
      if(inList(clientCredentialsMethods, method)) {
    	  token = this.credentialsManager.getCurrentToken();
    	  log("using clientCredentials token " + token);
      }
      

      
      if (null != token && token.length() > 0) {
        authorized = true;
        requestJSON.put(ServiceProviderConstants.TOKEN, token);

      } else if (AttConstants.PROVIDER.equals(action) && inList(whiteList, method)) {
        authorized = true;
      } else {
    	responseJSON.put(AttConstants.TYPE, AttConstants.EXCEPTION);
        responseJSON.put(ServiceProviderConstants.ERROR, "Unauthorized request");
      }

      if (authorized) {
        requestJSON.put(ServiceProviderConstants.HOST, AttConstants.HOST);
        requestJSON.put(ServiceProviderConstants.CLIENTID, getClientID());
        requestJSON.put(ServiceProviderConstants.CALLBACK, AttConstants.CALLBACK_SERVER);
        requestJSON.put(ServiceProviderConstants.SHORTCODE, getShortCode());

        try {
          String modifiedMethod = ServiceProviderConstants.GET + method;
          Method foundMethod = DirectServiceProvider.class.getMethod(modifiedMethod.toLowerCase(), JSONObject.class);
          responseJSON = (JSONObject) foundMethod.invoke(DirectServiceProvider.class, requestJSON);
        } catch (NoSuchMethodException nsme) {
          responseJSON.put(ServiceProviderConstants.ERROR, "Unrecognised method");
        } catch (Exception e) {
          responseJSON.put(ServiceProviderConstants.ERROR, e.getMessage());
        }
        if (null == responseJSON.optString(AttConstants.TYPE)) {
          responseJSON.put(AttConstants.TYPE, AttConstants.EXCEPTION);

        } else {
          responseJSON.put(AttConstants.TYPE, AttConstants.RPC);
        }
        responseJSON.put(AttConstants.TID, requestJSON.get(AttConstants.TID));
        responseJSON.put(AttConstants.ACTION, action);
        responseJSON.put(AttConstants.METHOD, method);
      }

      responseJSON.write(out);
    } catch (Exception se) {
      try {
        TokenResponse.getResponse(se).write(out);
      } catch (Exception e) {
        log(se.getMessage());
        e.printStackTrace();
      }
    } finally {
      out.flush();
      out.close();
    }
  }

  private String getClientID() {
    return AttConstants.CLIENTIDSTRING;
  }

  private String getClientSecret() {
    return AttConstants.CLIENTSECRETSTRING;
  }

  private String getShortCode() {
    return AttConstants.SHORTCODE;
  }

  private String getHost(HttpServletRequest request) {
    return "http://" + request.getServerName() + ":" + request.getServerPort();
  }

  private boolean inList(String[] stringArray, String name) {
    List<String> list = Arrays.asList(stringArray);
    Set<String> set = new HashSet<String>(list);
    return set.contains(name);
  }

  private JSONObject getData(HttpServletRequest request) throws JSONException {
    StringBuffer jb = new StringBuffer();
    String line = null;
    try {
      BufferedReader reader = request.getReader();
      while ((line = reader.readLine()) != null)
        jb.append(line);
    } catch (Exception e) {
      return new JSONObject().put(AttConstants.ERROR, e.getMessage());
    }
    return new JSONObject(jb.toString());
  }

}