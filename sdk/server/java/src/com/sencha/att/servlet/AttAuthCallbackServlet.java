package com.sencha.att.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.sencha.att.AttConstants;
import com.sencha.att.provider.ServiceProviderConstants;
import com.sencha.att.provider.ServiceProviderOauth;
import com.sencha.att.provider.TokenResponse;

/**
 * Once the user has logged in with their credentials, they get redirected to
 * this URL with a 'code' parameter. This is exchanged for an access token which
 * can be used in any future calls to the AT&T APIs
 */
public class AttAuthCallbackServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  
  private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);


  /**
   * @see HttpServlet#HttpServlet()
   */
  public AttAuthCallbackServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doPost(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 
	  
	String code = request.getParameter(AttConstants.CODE);

	log.info("Auth Callback " +code);
	
	JSONObject results = new JSONObject();
	
	if(code != null) {
		ServiceProviderOauth provider = new ServiceProviderOauth(AttConstants.HOST, getClientID(), getClientSecret(),
				getHost(request));
		TokenResponse tokenres = provider.getToken(code);

		response.setContentType("text/html");



		if (!tokenres.hasError() && tokenres.getAccessToken().length() > 0) {

			log.info("Putting token in session " + tokenres.getAccessToken());

			request.getSession().setAttribute(ServiceProviderConstants.TOKEN, tokenres.getAccessToken());
			//forward(AttConstants.CALLBACKHTML, request, response);
			try {
				results.put("success", true);
				results.put("msg", "Process Callback");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			//forward(AttConstants.CALLBACKHTML, request, response);
			try {
				results.put("success", false);
				results.put("msg", "No auth code");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	} else {
		//Check for errors
		try {
			results.put("success", false);
			results.put("error",request.getParameter("error"));
			results.put("error_reason",request.getParameter("error_reason"));
			results.put("error_description",request.getParameter("error_description"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
   
    
    Writer out = response.getWriter();
    try {
    	
    	out.write(AttConstants.REDIRECT_HTML_PRE);
    	
		results.write(out);

    	out.write(AttConstants.REDIRECT_HTML_POST);
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    out.flush();
    out.close();

  }

  

  private String getClientID() {
    return AttConstants.CLIENTIDSTRING;
  }

  private String getClientSecret() {
    return AttConstants.CLIENTSECRETSTRING;
  }

  private String getHost(HttpServletRequest request) {
    return "http://" + request.getServerName() + ":" + request.getServerPort();
  }
}
