package com.sencha.att.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.sencha.att.AttConstants;
import com.sencha.att.provider.ClientCredentialsManager;
import com.sencha.att.provider.Payment;
import com.sencha.att.provider.ServiceProviderConstants;
import com.sencha.att.provider.ServiceProviderOauth;
import com.sencha.att.provider.TokenResponse;

/**
 * Once the user has logged in with their credentials, they get redirected to
 * this URL with a 'code' parameter. This is exchanged for an access token which
 * can be used in any future calls to the AT&T APIs
 */
public class PaymentCallbackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(ServiceProviderConstants.SERVICEPROVIDERLOGGER);


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PaymentCallbackServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String transactionAuthCode = request.getParameter(AttConstants.TransactionAuthCode);

		log.info("Transaction Auth Code " + transactionAuthCode);

		JSONObject results = new JSONObject();
		try {
			if(transactionAuthCode != null) {

				results.put("success", true);
				results.put("TransactionAuthCode", transactionAuthCode);

			} else {
				//Check for errors

				results.put("success", false);
				results.put("error",request.getParameter("error"));
				results.put("error_reason",request.getParameter("error_reason"));
				results.put("error_description",request.getParameter("error_description"));

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Writer out = response.getWriter();
		try {

			out.write(AttConstants.REDIRECT_HTML_PRE);

			results.write(out);

			out.write(AttConstants.REDIRECT_HTML_POST);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.flush();
		out.close();

	}
}
