package com.sencha.att.servlet;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.sencha.att.AttConstants;

/**
 * Return a json object with either 'true' or 'false' depending on whether an
 * access_token has been set. This servlet will check the servelt session for the presence of an auth token
 * This indicates whether the user is logged in and can make api calls to the ATT API.
 */
public class AttAuthCheckServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public AttAuthCheckServlet() {
    super();
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doPost(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   *      response)
   */

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      JSONObject object = new JSONObject();
      
      if(AttConstants.STUBMODE) {
    	   request.getSession().setAttribute("token", "1111111111111");
      }

 
      String token = (String) request.getSession().getAttribute("token");
      
      
      
      if (null != token && token.length() > 0) {
        object.put("authorized", true);
      } else {
        object.put("authorized", false);
      }
      Writer out = response.getWriter();
      object.write(out);
      out.flush();
      out.close();
    } catch (JSONException se) {
      try {
        Writer out = response.getWriter();
        JSONObject resp = new JSONObject();
        resp.put(AttConstants.ERROR, se.getMessage());
        resp.write(out);
        out.flush();
        out.close();

      } catch (Exception e) {
        log(se.getMessage());
        e.printStackTrace();
      }    }
  }
}
