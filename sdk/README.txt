AT&T API Platform SDK for HTML5
====


The AT&T API Platform SDK for HTML5 provides an easy way to develop Sencha Touch applications that can access the AT&T APIs using javascript. The HTML5 experience for AT&T developers focuses on an enhanced capability to leverage the AT&T network-based services to create feature-rich apps. Access to services like SMS, Terminal Location, and MMS in conjunction with the ability to enable direct carrier billing provide developers with a multitude of options not normally available to a mobile web application developers.


Payments
---
The AT&T sample Payment application that uses the New Transaction and the Get Transaction Status methods may be used by Developers to build Payment applications. Other Payment API methods and related functionality are not currently supported for Developer's use within their applications at this time. Please contact AT&T Developer Support Program for details.


To get started open docs/index.html in a web browser. 


