Java Setup
===

A version of the JDK needs to be installed on the computer running the example application.

You only need Ant if you plan to build and run the application from the command line.  If you aren't going to use the command line, or if you don't have cigwin setup on windows to execute shell scripts, we recommend you use Eclipse to build and run the application.

JDK
---

Download and install Java for your platform:

<http://www.oracle.com/technetwork/java/javase/downloads/index.html>

Oracle offers many flavors of java, however the base JDK in any of the distributions are sufficient to run the examples.

Ant
---

Apache has a complete set of install instructions for all platforms.

<http://ant.apache.org/manual/install.html>

Running from Eclipse
---

Download and install Eclipse. Any of the java development versions will work.

<http://www.eclipse.org/downloads/>

