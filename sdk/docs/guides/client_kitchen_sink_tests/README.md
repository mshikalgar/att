Testing the Kitchen Sink 
===



Now that you have completed setting up the server component of the SDK you should be able to load the kitchen sink in your supported web browser.

When you load the home screen you should see this:

![Home](resources/images/test-screenshots/home.png)


Oauth Tests
---
Both Device Info and Device Location require your user to authorize access via an oauth login.  The SDK automates this process by presenting the user with an access screen then capturing and storing the users authentication token in the servers session for that user.  You only have to authenticate once per session.  The first time you click on either Device Info or Device Location you will be presented with the oauth login screen as a popup:


![Oauth](resources/images/test-screenshots/oauth-one.png)

There are a variety of authentication methods you can use, but the easiest if you have an AT&T phone with an active number, is entering your number into the "Wireless Number" field and clicking "Allow".

From there you will be redirected to a screen where you can enter a pin.  You should receive an SMS message on your phone with the pin.
Enter the pin you get from the text message:


![Oauth](resources/images/test-screenshots/oauth-pin.png)

Assuming that you entered the correct pin you should see a close screen indicating that you have authorized the request:


![Oauth](resources/images/test-screenshots/oauth-close.png)

Make sure to click close because its only after you click close that the user is redirected back to your application and the auth token is delivered to the server.


Test Device Info
---

After you have completed oauth you can click on the Device Info link from the home screen:

![Home-Location](resources/images/test-screenshots/home-info.png)

Then you can enter the AT&T mobile number you want information on:

![Device-info](resources/images/test-screenshots/device-info.png)


The loading screen should appear and after a few seconds you should see the results screen:
![Device-info](resources/images/test-screenshots/device-info-result.png)

Tap/click outside the dialog to close.




Test Device Location
---

After you have completed oauth you can click on the Device Location link from the home screen:

![Home-Location](resources/images/test-screenshots/home-location.png)

Then you can enter the AT&T mobile number you want a location on:

![Device-info](resources/images/test-screenshots/device-location.png)


The loading screen should appear and after a few seconds you should see the results screen:
![Device-info](resources/images/test-screenshots/device-location-result.png)

Tap/click outside the dialog to close.



Test SMS
---
From the API list click on the SMS link:

![Home-SMS](resources/images/test-screenshots/home-sms.png)

You should now see this screen:


![Home](resources/images/test-screenshots/sms.png)

Enter an AT&T phone number into the text box and click send SMS.

The loading screen should appear and after a few seconds you should see the results screen:
![SMS Results](resources/images/test-screenshots/sms-result.png)

If successful you will see the JSON data for the sent SMS or you will see an error message if something went wrong.

Tap/click outside the dialog to close.

Next check the status of your SMS message by clicking on the SMS status button:

![SMS Status](resources/images/test-screenshots/sms-status.png)

You will see the current status of the SMS message in JSON format.


Test MMS
---
From the API list click on the MMS link:

![Home-MMS](resources/images/test-screenshots/home-mms.png)

You should now see this screen:

![Home](resources/images/test-screenshots/mms.png)

Enter an AT&T phone number into the text box and click Send MMS.

The loading screen should appear and after a few seconds you should see the results screen:
![MMS Results](resources/images/test-screenshots/mms-result.png)

If successful you will see the JSON data for the sent MMS or you will see an error message if something went wrong.

Next check the status of your MMS message by clicking on the MMS status button:

![mms Status](resources/images/test-screenshots/mms-status.png)

You will see the current status of the MMS message in JSON format.


Test WAPPush
---
From the API list click on the WAP link:

![Home-WAP](resources/images/test-screenshots/home-wap.png)

You should now see this screen:

![Home](resources/images/test-screenshots/wap.png)

Enter an AT&T phone number into the text box and click New Wap Push. WAP push is only supported on feature phones or non smart phones.  Android and iOS devices can't receive WAP messages. Use SMS and MMS instead.

The loading screen should appear and after a few seconds you should see the results screen:
![WAP Results](resources/images/test-screenshots/wap-result.png)


