Installing and configuring an Apache server with PHP
====

There are many different ways to configure Apache and its virtual hosts. In this document we provide an example of how to setup a virtual server on your local development environment.

Instructions
---
Apache, PHP and cURL are required. If not already installed see instructions below.

If you need to install Apache and PHP you can use our instructions below and our virtual hosting configuration from above.


Install Apache, PHP and cURL (Windows XP SP3)
---

If Apache and PHP aren't already installed, installing XAMPP 1.7.3 provides both:

<http://sourceforge.net/projects/xampp/files/XAMPP%20Windows/1.7.3/>

Once installed, uncomment this line in xampp/php/php.ini and restart Apache to enable cURL:

    extension=php_curl.dll


Note: When you make changes to the apache configuration you must restart your server. After adding a new virtual host make sure to restart Apache with the XAMPP control panel.

Now you can return to the  "Create a new Virtual Host Configuration" section of [PHP Getting Started Guide](#!/guide/server_php) to add a virtual host to your server setup.

Install Apache, PHP and cURL (Ubuntu 11.04)
---

Use this command to install Apache and PHP:

    sudo apt-get install apache2 mysql-server php5 libapache2-mod-php5 php5-xsl php5-gd php-pear libapache2-mod-auth-mysql php5-mysql

Use this command to install cURL for PHP

    sudo apt-get install php5-curl

Note: When you make changes to the Apache configuration you must restart your server. After adding a new virtual host make sure to restart Apache with this command:

    sudo /etc/init.d/apache2 restart

Now you can return to the "Create a new Virtual Host Configuration" section of [PHP Getting Started Guide](#!/guide/server_php) to add a virtual host to your server setup.



Install Apache, PHP and cURL (OSX 10.6.3)
---

If Apache and PHP are not already installed, installing MAMP 2.0 provides both:

<http://www.mamp.info>

cURL is included in OSX

Note: When you make changes to the apache configuration you must restart your server. After adding a new virtual host make sure to restart apache with the MAMP control panel.

Now you can return to the "Create a new Virtual Host Configuration" section of [PHP Getting Started Guide](#!/guide/server_php) to add a virtual host to your server setup.
