Getting Started
=======

The AT&T sample Payment application that uses the NewTransaction and the GetTransactionStatus methods may be used by Developers to build Payment applications. The other Payment API methods and related functionality are not available for Developer's to use within their applications at this time. Please contact AT&T Developer Support Program for details.

The AT&T API Platform SDK for HTML5 provides an easy way to develop Sencha Touch applications that can access the AT&T APIs using javascript. The HTML5 experience for AT&T developers focuses on an enhanced capability to leverage the AT&T network-based services to create feature-rich apps. Access to services like SMS, Terminal Location, and MMS in conjunction with the ability to enable direct carrier billing provide developers with a multitude of options not normally available to a mobile web application developers.


Overview
---

AT&T API Platform SDK for HTML5 has three main layers: Client/Browser, HTML5 SDK Server and AT&T API Platform:

![overview](resources/images/att-overview.png)

**Client/Browser**, in this layer lives your application code that the end user interacts with. The AT&T SDK for HTML5 uses Sencha Touch to give application developers an easy way to develop cross platform mobile web apps. Also included in the SDK is Sencha Direct. It provides a Remote Procedure Call (RPC) framework that connects javascript code running in the browser to server side function calls. The SDK uses Sencha Direct to send AT&T API requests from the Browser to the  SDK Server. On top of Sencha Direct the SDK exposes ATT.Provider. ATT.Provider is what the application developer can use to make API calls to the AT&T API Platform. For complete documentation on Sencha Direct please visit the [Sencha Direct website](http://www.sencha.com/products/extjs/extdirect).

**HTML5 SDK Server**, provides reusable and extendable server code written in Java, PHP and Ruby. The SDK server takes requests from ATT.Provider and forwards them to the AT&T API Platform.


**AT&T API Platform** The AT&T services that the application can call.


Payments
---
The AT&T sample Payment application that uses the New Transaction and the Get Transaction Status methods may be used by Developers to build Payment applications. Other Payment API methods and related functionality are not currently supported for Developer's use within their applications at this time. Please contact AT&T Developer Support Program for details.


Next Steps
---

This guide provides step by step instructions on how to configure and launch an application server in Java, Ruby, or PHP. Once your application server is running you will be able to launch the included Kitchen Sink Application in a webkit based browser.

The Kitchen Sink is a Sencha Touch application which runs in your browser. It communicates with an application server which then communicates with AT&T's servers to make API requests on behalf of the user.

This guide is meant for intermediate to advanced web application developers who have a good understanding of html, javascript and related browser and server technologies. We provide server implementations of the SDK in Java, Ruby, or PHP. Which ever language you choose to deploy on you should be familiar with installing, configuring and developing applications in that language.

All of the client-side code is written in JavaScript using the Sencha Touch SDK. This guide is designed to get the application up and running making API calls using the Kitchen Sink. If you want to customize the behavior of the Sencha Touch based Kitchen Sink or develop custom applications using the provided javascript code you will need to become familiar with the Sencha Touch SDK. To gain a better understanding of Sencha Touch, please visit [Sencha Learn](http://www.sencha.com/learn/touch/) where you will find in depth tutorials and API documentation.


**You will need a webkit based browser to run the kitchen sink. Desktop Webkit browsers include Google Chrome and Apple Safari. Supported mobile devices include Android and iOS. **



The first thing you will need to do before you can start making API calls is to setup an application on the [AT&T Developer Program](http://developer.att.com/).


How to Create an Application
----

Once you login to the developer program you can create an application in the [My Apps](http://devconnect-api.att.com/) sections of the site.

When you create an application you will be given a set of credentials that are used to configure the SDK.

For this example we use all of the AT&T service APIs: Multimedia Messaging Service (MMS), Short Messaging Service (SMS), WAP Push, Device Capability, Terminal Location, and Payment.

As you create your application, enable all of these services.

OAuth Redirect URL:
---

When creating the application, an OAuth Redirect URL is required.

This is the URL to your application; when the user has completed the login process they are redirected here. When the application attempts an OAuth login, AT&T's servers check to make sure the passed callback matches the one provisioned for your application in this step.

The HTML5 SDK is using iframes to handle the OAuth login, because of this cross-domain access controls come into effect.
When the AT&T API redirects the user (in the iframe) back to http://yourhost:yourport/att/callback yourhost:yourport will need to match exactly with the yourhost:yourport where the application was loaded.


If your application is hosted on mynewapp.com and the user loads mynewapp.com then your redirect url will need to be http://mynewapp.com/att/callback

In these examples and guides we assume that you are using a host of 127.0.0.1 (your local development server).  However, any valid DNS name will work. If you plan on testing the client on a different computer or on a mobile device, using 127.0.0.1 will not work. In this case you will need to use a real DNS name that maps to your server.

Once you have your application provisioned with the AT&T developer program you can proceed with configuring the SDK.


SDK Server
----

The Sencha SDK provides examples to address cross domain access and other security concerns. The SDK provides a proxy between the Sencha Touch application and AT&T API. It also provides convenience methods to request the OAuth login sequence, fetch an access token, and make requests to the AT&T APIs.

The SDK has been implemented in three popular languages: Java, Ruby and PHP.

The implementation in each language provides a consistent HTTP API for Client access. The API allows Clients to connect to any of the server implementations without modification.

The code is designed to run with a minimum number of external dependencies, making integration into your specific environment as convenient as possible. However, depending on the specific language and development tools used in your environment, modifications may be required to the provided proxy code.


Quick Start
---

Choose the server guide from the left for your language of choice. It will walk you through the process of running a Sencha Touch application that uses the AT&T API Platform.