Sencha Java SDK
===

To simplify the deployment of the SDK using java we have included an embedded Jetty servlet container.  The Java code is built using Apache Ant.

Prerequisites
----

Java version 1.6 or later. You can obtain this from http://java.com.

Optional:
Ant version 1.8 or later
Eclipse IDE for Java Developers


If you can execute the following commands from the command line, you should be able to build and run the java server:


    # java -version
    java version "1.6.0_24"

    # ant -version
    Apache Ant(TM) version 1.8.2 compiled on February 28 2011


If you are using Ant: The SDK download includes the file att.jar; this can be used to run the example. However if you want to automatically generate a .war file for deployment with a different servlet container, you can use the provided ant build file.


If you are using the Eclipse IDE: We have provided the .project and .classpath files needed to automatically create an eclipse project. You can then build and run the application from within Eclipse, (you won't need the command line instructions).


**Windows Users** We recommend you use the **Build and Run using Eclipse** instructions below as you can build and run the SDK using eclipse without needing to configure a command line environment.


Java Server Documentation
---
The SDK download includes javadoc formatted documentation in [sdk/server/java/docs](../server/java/docs).


Server configuration
---

Open the file conf/att-api.properites and find the following settings:

    # Replace these values with apiKey, secretKey and short code
    # for your application in devconnect.
    apiKey=XXXXXX
    secretKey=XXXXXX
    shortCode=XXXXXX

    # the OAuth Redirect Url setup above.
    authCallbackUrl=http://127.0.0.1:8080/att/callback

    # This is the main endpoint through which all API requests are made
    apiHost=https://api.att.com


Command Line (Mac, Linux, Unix)
====

Run using the command line
---

Once the source code has been built and the .war file created you can run the application using Jetty.  If you want to deploy the .war in a different Java servlet container such as Tomcat you can copy the .war file from  dist/att.war into your servlet container's deployment directory and follow your vendor's instructions for deploying .war files.


To run the application using the included jetty:

    # sh run.sh


The application should now be running on http://yourhost:8080/

To change the port number pass a different port as the first argument:

    # sh run.sh 8484


Because Jetty is running as an interactive process you will need to kill the process manually using control-c.
You can background run.sh using the '&', just remember to manually kill the process when you are done with it.

**Please note that we don't recommend deploying your application using the provided embedded jetty.** It should be used for development and testing purposes only.
If you want your application to run for an extended period of time we recommend deploying the application as a .war file see "Deploy the .war File" below.
That way you can take advantage of the deployment management options on your application server.




Build using the command line (Apache Ant)
---

To build the application using Ant from the command line:

    # sh build.sh

A directory called *dist* will be created an __att.jar__ and __att.war__ file will be created.


Details on the ant build file can be found here.





Eclipse IDE (Mac, Linux, Unix, Windows)
====

Build and Run using Eclipse
---

With eclipse running create a new project using File->New->Project.

![overview](resources/images/eclipse-new.png)

 - On the "Select a Wizard" screen choose "Java Project"
 - Click Next
 - Give the project a name.
 - Uncheck "Use default location"
 - Click the browse button to location the SDK/server/java directory on your computer.
 - Choose the SDK/server/java directory.
 - The rest of the new project settings can be left with their default setting. our included .project and .classpath will correctly configure the project.
 - Click Finish

Once the project is created it should build automatically without error.

Using the package *explorer* expand com.sencha.jetty and open EmbeddedServer.

From the Run menu choose the Run command.

If you have the console view open you should see output similar to the following:

        2011-11-08 14:23:37.142:INFO:oejs.AbstractConnector:Started SelectChannelConnector@0.0.0.0:8080 STARTING
        2011-11-08 14:23:41.555:INFO:/:AttDirectRouter: AT&T Provider initialized.
        2011-11-08 14:23:41.555:INFO:/:AttDirectRouter:
        2011-11-08 14:23:41.557:INFO:/:AttDirectRouter: API endpoint:  https://api.att.com
        2011-11-08 14:23:41.557:INFO:/:AttDirectRouter: Client ID:     0d335f38dd5c0e8da0c569211563c099dddd
        2011-11-08 14:23:41.557:INFO:/:AttDirectRouter: Client Secret: 2dc632bcd4628137
        2011-11-08 14:23:41.557:INFO:/:AttDirectRouter: Shortcode:     37367231


The application should now be running on http://yourhost:8080/

When you are done using running the server simply stop the process (using the stop button from Eclipse's console view or debug view).


Ant build using Eclipse
---

Eclipse provides an easy way to run ant builds. Right-Click (command click, etc) on the **build.xml** in the Package Explore to open the contextual menu
Select Run As -> Ant Build...
The default target will build the entire SDK and produce a .war file.
Choose a target and click **Run**.


Deploy the .war File
====


The ant build.xml provided by the SDK creates a deployable .war file.  It will bundle the client directory, java code, the contents of **webapp/WEB-INF/**.

**att-api.properties** will be copied to **WEB-INF/classes/** so you need to configure your application before creating the build.

Follow the directions in **Build using the command line (Apache Ant)** and copy the **att.war** to your application server. Every application server deployment is different but the build .war is self contained and should deploy on any standard compliant application server.

Alternate locations for att-api.properties
---

To provide some flexibility in the deployment of the application there are two ways to load the properties file. The default is to look on the classpath using the classloader for  **att-api.properties**  You can override this behavior by specifying a java system property. By specifying a system property the application can be re-configured without recompiling the .war file.

**com.sencha.example.servlet.AttConstants** will look first for a system property **att.api.conf**

    String apiFile = System.getProperty("att.api.conf");

If it finds **att.api.conf** it will assume that its value is the full path to the att config file.

if **att.api.conf** is not found, then it will attempt to load the file using the classloader:

    Thread.currentThread().getContextClassLoader().getResourceAsStream("att-api.properties");

If neither of these methods will meet your deployment needs you will need to modify **AttConstants** to load your configuration files from an alternate location.