Sencha PHP SDK
===

This guide provides instructions to create a web application using php running in an apache web server. At the end of this guide you will have a Sencha Touch web application that can connect to the AT&T APIs.


Prerequisites
----

- Apache 2.2
    - **MultiViews** must be enabled on the 'server' portions of the apache configuration file. The client makes API calls that look like /att/some_call but the files on the file system are /att/some_call.php. MultiViews is the simple way to have apache do the mapping.
- PHP 5.3.1
 - PHP 5.3.1 must have cURL support enabled. To test this, create a script that calls `phpinfo()` and point your browser to it to see your current PHP configuration.
 - In your php.ini file, the ___short_open_tag___ setting should be to ___On___.


If you don't have access to a running apache server with PHP that meets these requirements see our [PHP Server Environment Setup](#!/guide/server_php_env) for instructions on how to install apache and php for Macintosh, Windows or Linux.

Review the documentation and account setup documents on Devconnect.


In these instructions ___[docroot]___ is the file path to the document root of your apache web server's virtual server.


It is recommended that you configure apache with a virtual host that only serves the example application.


Create a new Virtual Host Configuration
---

If you already have an apache virtual server configured you can skip this step.  However please compare our virtualhost configuration to yours to ensure that you have all of the needed settings.

In our example we leave the client and server directories in their original location and use mod_alias to map the server code under an 'att' virtual directory.

    <Directory "[Yourpathtothesdk]/sdk/client">
            Options -Indexes MultiViews
            AllowOverride None
            Order allow,deny
            Allow from all
    </Directory>

    <Directory "[Yourpathtothesdk]/sdk/server/php/public_html">
            Options -Indexes MultiViews
            AllowOverride None
            Order allow,deny
            Allow from all
    </Directory>

    NameVirtualHost *:PORT

    <VirtualHost *:PORT>
            ServerName att.dev.local
            DocumentRoot "[Yourpathtothesdk]/sdk/client"

            Alias /att [Yourpathtothesdk]/sdk/server/php/public_html/att

    </VirtualHost>

We specify a named virtual server using ServerName att.dev.local which is a locally mapped domain name. You can either add a real DNS based host name or modify your /etc/hosts file to include it.

Replace tokens:

* Replace both occurrences of PORT with the port number on which apache is currently running.
* [Yourpathtothesdk] with the full path to your sdk directory.
* replace att.dev.local with the dns name you will be using

Restart your apache server once you have added the virtual host for the changes to take effect.

Add the modified configuration to your systems apache config file.

Use an existing Virtual Host Configuration
---
Assuming that you have an existing VirtualHost, you can copy the server and client directories into an existing virtual host.  Make sure that there aren't any existing files in your  ___[docroot]___ that conflict with file and directory names in the SDK or they will be overwritten.

Copy client and server files to your web server:

 * Copy the contents of **sdk/server/php/public_html** to ___[docroot]___.
 * Copy the contents of **sdk/client** to ___[docroot]___.



Server configuration
---

Once you have configured your virtual server you will need to configure you application.  To pass this step you must have configured an application in devconnect.

Open ___[docroot]___/config.php you will find the following settings:

    $provider = ProviderFactory::init(array(
      "provider" => "att",

      # Client ID and Secret from the AT&T Dev Connect portal.
      "apiKey" => "XXXXXX",

      "secretKey" => "XXXXXX",

      # The address of the locally running server. This is used when a callback URL is
      # required when making a request to the AT&T APIs.
      "localServer" => "http://127.0.0.1:8888",

      # The shortcode is the SMS number used when sending and receiving SMS and MMS messages.
      "shortcode" => "XXXXXX",

      # This is the main endpoint through which all API requests are made
      "apiHost" => "https://api.att.com"


    ));

Modify the configuration settings to match the application you created in devconnect.

Logging to a dedicated log file can also be configured in config.php

    define("DEBUG", "1");
    define("DEBUG_LOGGER", "/some/writable/absolute/path/att-php.log");


Running the Application
---
Your application should now be configured and ready to use. Open http://[yourhost]:[yourport]/ in a supported browser.