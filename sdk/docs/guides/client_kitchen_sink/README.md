Kitchen Sink
===

The Kitchen Sink is a simple Sencha Touch application that exercises the AT&T APIs. The details of each JavaScript class are covered in KitchenSink.application

![KS](resources/images/test-screenshots/home.png)


If you would like to see a good general introduction on how to build a Sencha Touch Application please see our [Sencha Touch Hello World](http://www.sencha.com/learn/hello-world)



Payments
---
The AT&T sample Payment application that uses the New Transaction and the Get Transaction Status methods may be used by Developers to build Payment applications. Other Payment API methods and related functionality are not currently supported for Developer's use within their applications at this time. Please contact AT&T Developer Support Program for details.


Code Organization
---
All of the client code is located in the folder **sdk/client**.  The server code located under **sdk/server** serves the static files under the client folder.

Entry into the application is **client/index.html**, it loads all of the CSS and JavaScript needed to run the application:

Standard Includes
---
To access the AT&T API and build Sencha Touch Application your application will need at a minimum three files.

Sencha Touch CSS:

    <link rel="stylesheet" href="resources/css/sencha-touch.css" type="text/css">

Sencha Touch debug build which is available in two versions (uncompressed and compressed for production use):

    <script type="text/javascript" src="sencha-touch-debug.js"></script>

Sencha Direct is the library used to facilitate communication between the client and the server:

    <script type="text/javascript" src="sencha-direct-debug.js"></script>

The Att.Provider library that communicates with the server:

    <script type="text/javascript" src="lib/AttProvider.js"></script>


Application Specific Include Files
---

The rest of the files included in index.html are specific to the Kitchen Sink Application.  We have separated each set of API calls into individual views.  If you are interested in the SMS related APIs then click on the KitchenSink.views.SMS link below. Each view will show you how to make API calls and what data you will receive after making the call.

Include KitchenSink.application

    <script type="text/javascript" src="app/app.js"></script>

include KitchenSink.controllers.index

    <script type="text/javascript" src="app/controllers/index.js"></script>


Include each of the KitchenSink views:

include KitchenSink.views.KitchenSink


    <script type="text/javascript" src="app/views/kitchenSink.js"></script>

include KitchenSink.views.ApiList


    <script type="text/javascript" src="app/views/apiList.js"></script>

include KitchenSink.views.DeviceInfo


    <script type="text/javascript" src="app/views/deviceInfo.js"></script>

include KitchenSink.views.DeviceLocation

    <script type="text/javascript" src="app/views/deviceLocation.js"></script>

include KitchenSink.views.MMS

    <script type="text/javascript" src="app/views/mms.js"></script>

include KitchenSink.views.Payments

    <script type="text/javascript" src="app/views/payments.js"></script>

include KitchenSink.views.SMS

    <script type="text/javascript" src="app/views/sms.js"></script>

include KitchenSink.views.WAP

    <script type="text/javascript" src="app/views/wap.js"></script>

