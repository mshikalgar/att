Simple Example Application: SMS
===

The SMS example is a simple Sencha Touch application that uses the AT&T API Platform SDK for HTML5 to send SMS messages to AT&T mobile numbers.

If you would like to see a good general introduction on how to build a Sencha Touch Application please see our [Sencha Touch Hello World](http://www.sencha.com/learn/hello-world)


Prerequisites
----

This guide assumes that you have already setup the SDK, provisioned an application with AT&T and have deployed the kitchen sink app using the SDK setup guides.


If you already have the SDK running you can run the SMS example by loading http://**yoursdkhost:yoursdkport**/examples/sms/index.html in your supported webkit browser. You should see a screen that looks like this:

![overview](resources/images/examples-sms.png)


Code Organization
---
All of the client code is located in the folder **sdk/client/examples/sms**.

Entry into the application is **client/examples/sms/index.html**, it loads all of the CSS and JavaScript needed to run the application:



    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
    <html>
      <head>
        <meta charset="utf-8">
        <title>Sencha AT&amp;T Test</title>
        <link rel="stylesheet" href="../../resources/css/sencha-touch.css" type="text/css">

        <script type="text/javascript" src="../../sencha-touch-debug.js"></script>

            <script type="text/javascript" src="../../sencha-direct-debug.js"></script>

        <script type="text/javascript" src="../../lib/AttProvider.js"></script>

        <script type="text/javascript" src="app/app.js"></script>

        <script type="text/javascript" src="app/views/sms.js"></script>
      </head>
      <body>
      </body>
    </html>





Standard Includes
---
To access the AT&T API and build a Sencha Touch Application your application will need at a minimum three files.  We are loading the same copies of these files as the kitchen sink in the client directory. When you build your application, keep a single copy of the JS and CSS files in a central location.

Sencha Touch CSS:

    <link rel="stylesheet" href="../../resources/css/sencha-touch.css" type="text/css">

Sencha Touch debug build which is available in two versions (uncompressed and compressed for production use):

    <script type="text/javascript" src="sencha-touch-debug.js"></script>

Sencha Direct is the library used to facilitate communication between the client and the server:

    <script type="text/javascript" src="../../sencha-direct-debug.js"></script>

The Att.Provider library that communicates with the server:

    <script type="text/javascript" src="../../lib/AttProvider.js"></script>


Application Specific Include Files
---

The rest of the files included in index.html are specific to the SMS Only Application.


    <script type="text/javascript" src="app/app.js"></script>


In app.js we register our application, initialize Att.Provider and create our default view:

        Ext.regApplication({
            name: "SmsOnly",

            launch: function() {

                this.provider = new Att.Provider();

                this.views.viewport = new this.views.SMS();


            },


        });

When we created ATT.Provider and assigned it to this.provider.  **this** is the SmsOnly which is a global variable we can access from any JavaScript function in our application.  We will be calling SmsOnly.provider.sendSms() later on in the example.

To Define our view that we included in sms.js

    <script type="text/javascript" src="app/views/sms.js"></script>

The SMS view allows the user to enter a MSISDN for a mobile device associated with their account. Only devices attached to their account can be entered. Any other number will generate an error.

    SmsOnly.views.SMS = Ext.extend(Ext.Panel, {

        fullscreen: "true",

        /**
         * Once we send an sms message we store the ID returned by the API
         * so that we can check its delivery status.
         */
        smsId: null,

         /**
           * Initializes the view placing a text input field for the MSISDN one and two
           * and buttons to initiate the APIs
           */
        initComponent: function() {

            var self = this; // We need a reference to this instance for use in callbacks


            /**
              * Declare our child UI elements
              */
            this.items = [{
                dockedItems: [{
                    xtype: 'toolbar',
                    dock: 'top',
                    title: 'SMS'
                }],
                items: [

                /*
                * Create a form child element which contains two text fields.
                                for for the MSISDN and one for the message text.
                */
                    { xtype:"form",
                      items:[{id: 'smsOne', xtype:'textfield', value: "", label:"To"},
                              {id: 'message', xtype:'textfield', value: "Hi how are you?",  label:"Message"}]},

                /*
                * Create a button which will send the sms message when pressed.
                */
                    {
                        xtype: 'button',
                        text: 'Send SMS',
                        style: 'margin: 10px;',

                        /*
                        * Add a function handler so that the button will know what to do when pressed.
                        *  see sendSmsHandler() below
                        */
                        handler: self.sendSmsHandler,
                        scope: self
                    },
                     /*
                      * Create a button which will check the status of the sent sms message when pressed.
                      */
                    {
                        xtype: 'button',
                        text: 'SMS Status',

                        /*
                        * Give this button an id so we can enable it once the message is sent.
                        */
                        id: 'sms-status-button',
                        style: 'margin: 10px;',

                         /*
                          * Add a function handler so that the button will know what to do when pressed.
                          *  see smsStatusHandler() below
                          */
                        handler: this.smsStatusHandler,
                        scope: this,

                        /*
                        * Set the button to disabled by default.
                        */

                        disabled: true
                    }            ]
            }];

            SmsOnly.views.SMS.superclass.initComponent.apply(this, arguments);

        },


        /**
         * pulls the MSISDN from the smsOne text field and calls
         * SmsOnly.provider.sendSms
         * on a success response the results are displayed using SmsOnly.showResults
         */
        sendSmsHandler: function() {
            var self = this,

                /*
                * Get references to both the MSISDN and the Message.
                */
                smsOne =  Ext.getCmp("smsOne"),
                message =  Ext.getCmp("message");


                console.log("send a sms ", smsOne.getValue(), message.getValue());


            // Show a 'loading' overlay mask
            self.setLoading(true);

             /*
             * call send sms with the address, message and two callback handlers.
             */
             SmsOnly.provider.sendSms({
                 address : smsOne.getValue(),
                 message : message.getValue(),
                 success : function(response) {
                      /*
                      * When the API returns the results of the API call we will turn off the loading mask
                      * and inspect the response and display the result to the end user.
                      */

                     self.setLoading(false);
                     console.log("sms send response", response);

                     /*
                     * If the response has an Id the SMS message has been sent and we can check its status.
                     * so we will store the ID in the smsId property of the view.
                     */
                     if(response && response.Id) {
                       Ext.Msg.alert('Message Sent', "Message sent to " + smsOne.getValue());
                       self.smsId = response.Id;
                       Ext.getCmp('sms-status-button').enable();
                     } else {
                       var details = "unknown";

                      /**
                       * Handle errors from AT&T API call. We will attempt to check the response details and
                       * display a meaningful response to the user.
                       */
                       if(response && response.requestError
                                                && response.requestError.serviceException
                                                && response.requestError.serviceException.text) {

                         details = response.requestError.serviceException.text;

                         details = details.replace("%1", response.requestError.serviceException.variables);

                       }
                       Ext.Msg.alert('Could not send Message', details);
                     }


                 },
                 failure : function(error) {
                     console.log("failure", error);
                     self.setLoading(false);
                     Ext.Msg.alert('Error', error);
                 }
             });

        },

        /**
         * After sending an sms message using sendSmsHandler or sendMultiSmsHandler a
         * smsId is returned by the API. That value is stored in this.smsId.
         * This method calls SmsOnly.provider.smsStatus using this.smsId to get delivery status of the message.
         * on a success response the results are displayed using SmsOnly.showResults
         */
        smsStatusHandler: function() {
            var self = this;

            self.setLoading(true);

            SmsOnly.provider.smsStatus({
                      smsId   : self.smsId,
                      success : function(response) {
                          self.setLoading(false);
                          console.log("sms Status response", response);
                          Ext.Msg.alert('Status', response.DeliveryInfoList.DeliveryInfo[0].DeliveryStatus);
                      },
                      failure : function(error) {
                          self.setLoading(false);
                          Ext.Msg.alert('Error', error);
                      }
                  });


        }

    });